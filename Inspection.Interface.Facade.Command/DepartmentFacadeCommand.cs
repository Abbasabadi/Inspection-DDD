﻿using Framework.Application.Command;
using Inspection.Interface.Facade.Contracts.Department.Command;

namespace Inspection.Interface.Facade.Command
{
    public class DepartmentFacadeCommand: IDepartmentFacadeCommand
    {
        private readonly ICommandBus _commandBus;

        public DepartmentFacadeCommand(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public void CreateDepartment(CreateDepartmentCommand command)
        {
            _commandBus.Dispatch(command);
        }
    }
}
