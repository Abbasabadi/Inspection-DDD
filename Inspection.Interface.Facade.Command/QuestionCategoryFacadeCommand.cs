﻿using Framework.Application.Command;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;

namespace Inspection.Interface.Facade.Command
{
    public class QuestionCategoryFacadeCommand: IQuestionCategoryFacadeCommand
    {
        private readonly ICommandBus _commandBus;

        public QuestionCategoryFacadeCommand(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public void Create(CreateQuestionCategoryCommand command)
        {
            _commandBus.Dispatch(command);
        }

        public void UpdateActivationStatus(ChangeQuestionCategoryActivationCommand command)
        {
            _commandBus.Dispatch(command);
        }
    }
}