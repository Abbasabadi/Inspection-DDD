﻿using Framework.Application.Command;
using Inspection.Interface.Facade.Contracts.Department.Command;
using Inspection.Interface.Facade.Contracts.Question.Command;

namespace Inspection.Interface.Facade.Command
{
    public class QuestionFacadeCommand: IQuestionFacadeCommand
    {
        private readonly ICommandBus _commandBus;

        public QuestionFacadeCommand(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public void CreateQuestion(CreateQuestionCommand command)
        {
            _commandBus.Dispatch(command);
        }

        public void ChangeQuestionActivation(ChangeQuestionActivationCommand command)
        {
            _commandBus.Dispatch(command);
        }

        public void UpdateQuestion(UpdateQuestionCommand command)
        {
            _commandBus.Dispatch(command);
        }
    }
}
