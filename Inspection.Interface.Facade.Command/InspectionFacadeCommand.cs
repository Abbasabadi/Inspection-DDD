﻿using System.Collections.Generic;
using Framework.Application.Command;
using Framework.Core.Events;
using Inspection.Domain.Contract;
using Inspection.Interface.Facade.Contracts.Inspection.Command;

namespace Inspection.Interface.Facade.Command
{
    public class InspectionFacadeCommand : IInspectionFacadeCommand
    {
        private readonly ICommandBus _commandBus;
        private readonly IEventListener _listener;

        public InspectionFacadeCommand(ICommandBus commandBus, IEventListener listener)
        {
            _commandBus = commandBus;
            _listener = listener;
        }

        public long CreateInspection(CreateInspectionCommand command)
        {
            long id = 0;
            _listener.Listen(new ActionEventHandler<InspectionCreated>(x => id = x.Id));
            _commandBus.Dispatch(command);
            return id;
        }

        public void PlaceQuestionAnswers(List<PlaceQuestionAnswerCommand> commands)
        {
            _commandBus.Dispatch(commands);
        }
    }
}