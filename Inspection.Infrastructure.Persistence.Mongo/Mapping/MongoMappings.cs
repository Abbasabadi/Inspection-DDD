﻿using Framework.MongoDb;
using Inspection.Domain.Models.Department;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Models.Question;
using QuestionCategoryId = Inspection.Domain.Models.QuestionCategory.QuestionCategoryId;

namespace Inspection.Infrastructure.Persistence.Mongo.Mapping
{
    public static class MongoMappings
    {
        public static void MapIdBases()
        {
            MappingHelper.ConfigIdBases<InspectionId>();
            MappingHelper.ConfigIdBases<DepartmentId>();
            MappingHelper.ConfigIdBases<QuestionId>();
            MappingHelper.ConfigIdBases<QuestionCategoryId>();
        }
    }
}