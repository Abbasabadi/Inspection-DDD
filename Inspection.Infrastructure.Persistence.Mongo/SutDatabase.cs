﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Infrastructure.Persistence.Mongo
{
    public static class SutDatabase
    {
        public static string CSIS = "CSIS";
        
    }

    public static class SutCollection
    {
        public static string Questions = "Questions";
        public static string QuestionCategories = "QuestionCategories";
        public static string Inspections = "Inspections";
        public static string Departments = "Departments";
    }
}
