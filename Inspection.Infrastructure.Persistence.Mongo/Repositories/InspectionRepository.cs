﻿using Framework.MongoDb;
using Inspection.Domain.Models.Inspection;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Persistence.Mongo.Repositories
{
    public class InspectionRepository : IInspectionRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Domain.Models.Inspection.Inspection> _inspectionCollection;

        public InspectionRepository(IMongoDatabase database)
        {
            _database = database;
            _inspectionCollection = _database.GetCollection<Domain.Models.Inspection.Inspection>("Inspections");
        }

        public void CreateInspection(Domain.Models.Inspection.Inspection inspection)
        {
            _inspectionCollection.InsertOne(inspection);
        }

        public void UpdateInspectionAnswers(Domain.Models.Inspection.Inspection inspection)
        {
            var filter = MongoBuilders<Domain.Models.Inspection.Inspection>.Filter(x => x.Id, inspection.Id);
            var update =
                MongoBuilders<Domain.Models.Inspection.Inspection>.Update(x=>x.QuestionAnswers, inspection.QuestionAnswers);
            _inspectionCollection.FindOneAndUpdate(filter,update);
        }

        public Domain.Models.Inspection.Inspection GetById(long id)
        {
            var filter = MongoBuilders<Domain.Models.Inspection.Inspection>.Filter(x => x.Id.DbId, id);
            return _inspectionCollection.Find(filter).First();
        }

        public long GetNextId()
        {
            return _database.GetNextSequenceFrom("InspectionSeq");
        }
    }
}