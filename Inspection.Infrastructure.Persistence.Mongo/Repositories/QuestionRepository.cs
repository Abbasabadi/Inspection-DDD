﻿using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Persistence.Mongo.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Question> _questionCollection;

        public QuestionRepository(IMongoDatabase database)
        {
            _database = database;
            _questionCollection = _database.GetCollection<Question>("Questions");
        }

        public void CreateQuestion(Question question)
        {
            _questionCollection.InsertOne(question);
        }

        public void UpdateQuesiton(Question question)
        {
            var filter = MongoBuilders<Question>.Filter(x => x.Id, question.Id);
            _questionCollection.ReplaceOne(filter, question);
        }

        public Question GetBy(long id)
        {
            var filter = MongoBuilders<Question>.Filter(x => x.Id.DbId, id);
            return _questionCollection.Find(filter).First();
        }

        public long GetNextId()
        {
            return _database.GetNextSequenceFrom("QuestionSeq");
        }
    }
}