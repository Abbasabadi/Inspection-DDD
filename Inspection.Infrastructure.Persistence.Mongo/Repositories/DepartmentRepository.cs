﻿using Framework.MongoDb;
using Inspection.Domain.Models.Department;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Persistence.Mongo.Repositories
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Department> _departmentCollection;

        public DepartmentRepository(IMongoDatabase database)
        {
            _database = database;
            _departmentCollection = _database.GetCollection<Department>("Departments");
        }

        public void CreateDepartment(Department department)
        {
            _departmentCollection.InsertOne(department);
        }

        //TODO : Integration Test This
        public long GetNextId()
        {
            return _database.GetNextSequenceFrom("DepartmentSeq");
        }
    }
}