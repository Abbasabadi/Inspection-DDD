﻿using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Persistence.Mongo.Repositories
{
    public class QuestionCategoryRepository: IQuestionCategoryRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<QuestionCategory> _questionCategoryCollection;
        public QuestionCategoryRepository(IMongoDatabase database)
        {
            _database = database;
            _questionCategoryCollection = _database.GetCollection<QuestionCategory>("QuestionCategories");
        }

        public void Create(QuestionCategory questionCategory)
        {
            _questionCategoryCollection.InsertOne(questionCategory);
        }

        public QuestionCategory GetBy(long questionCategoryId)
        {
            var filter = MongoBuilders<QuestionCategory>.Filter(x => x.Id.DbId, questionCategoryId);
            return _questionCategoryCollection.Find(filter).First();
        }

        public void Update(QuestionCategory questionCategory)
        {
            var filter = MongoBuilders<QuestionCategory>.Filter(x => x.Id, questionCategory.Id);
            _questionCategoryCollection.FindOneAndReplace(filter, questionCategory);
        }

        public long GetNextId()
        {
            return _database.GetNextSequenceFrom("QuestionCategorySeq");
        }
    }
}
