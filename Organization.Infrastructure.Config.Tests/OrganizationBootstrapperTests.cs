﻿using System;
using Castle.Windsor;
using Organization.Domain.Models.Organizations;
using Organization.Interface.Facade.Contract.Organizations.Command;
using Organization.Interface.Rest.Api;
using Xunit;

namespace Organization.Infrastructure.Config.Tests
{
    public class OrganizationBootstrapperTests
    {
        private readonly IWindsorContainer _container;

        public OrganizationBootstrapperTests()
        {
            _container = new WindsorContainer();
        }

        [Fact]
        public void Wire_Up_Should_Register_Interfaces_With_Their_Implementaitions()
        {
            //Arrange

            //Act
            OrganizationBootstrapper.WireUp(_container);

            //Assert
            Assert.True(IsComponentRegistered(typeof(OrganizationController)));
            Assert.True(IsComponentRegistered(typeof(IOrganizationRepository)));
            Assert.True(IsComponentRegistered(typeof(IOrganizationFacadeCommand)));
            Assert.False(IsComponentRegistered(typeof(ICloneable)));
        }

        private bool IsComponentRegistered(Type component)
        {
            return _container.Kernel.HasComponent(component);
        }
    }
}