﻿using System.Collections.Generic;
using Framework.MongoDb;
using MongoDB.Driver;
using Organization.Domain.Models.Organizations;

namespace Organization.Infrastructure.Persistence.Mongo
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Domain.Models.Organizations.Organization> _organizationCollection;

        public OrganizationRepository(IMongoDatabase database)
        {
            _database = database;
            _organizationCollection =
                _database.GetCollection<Domain.Models.Organizations.Organization>("Organizations");
        }

        public void CreataOrganization(Domain.Models.Organizations.Organization organization)
        {
            _organizationCollection.InsertOne(organization);
        }

        public long GetNextId()
        {
            return _database.GetNextSequenceFrom("OrganizationSeq");
        }
    }
}