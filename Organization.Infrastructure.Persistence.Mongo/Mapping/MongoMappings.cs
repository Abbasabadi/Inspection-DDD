﻿using Framework.MongoDb;
using Organization.Domain.Models.Organizations;

namespace Organization.Infrastructure.Persistence.Mongo.Mapping
{
    public static class MongoMappings
    {
        public static void MapIdBases()
        {
            MappingHelper.ConfigIdBases<OrganizationId>();
        }
    }
}
