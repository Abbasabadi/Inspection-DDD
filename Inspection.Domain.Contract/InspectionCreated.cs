﻿using Framework.Core.Events;

namespace Inspection.Domain.Contract
{
    public class InspectionCreated : DomainEvent
    {
        public long Id { get; set; }

        public InspectionCreated(long id)
        {
            Id = id;
        }
    }
}