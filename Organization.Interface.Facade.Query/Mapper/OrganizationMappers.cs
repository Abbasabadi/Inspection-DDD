﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Organization.Interface.Facade.Contract.Organizations.Query;

namespace Organization.Interface.Facade.Query.Mapper
{
    public static class OrganizationMappers
    {
        public static OrganizationViewModel Map(Domain.Models.Organizations.Organization organization)
        {
            return new OrganizationViewModel
            {
                Id = organization.Id.DbId,
                Name = organization.Name,
                IsHeadQuarter = organization.IsHeadQuarter
            };
        }

        public static List<OrganizationViewModel> Map(List<Domain.Models.Organizations.Organization> organizations)
        {
            return organizations.Select(Map).ToList();
        }
    }
}
