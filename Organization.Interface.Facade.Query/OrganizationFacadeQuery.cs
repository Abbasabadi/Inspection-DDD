﻿using System.Collections.Generic;
using Organization.Interface.Facade.Contract.Organizations.Query;
using Organization.Interface.Facade.Query.Mapper;
using Orgnaization.Infrastructure.Query.Mongo;

namespace Organization.Interface.Facade.Query
{
    public class OrganizationFacadeQuery : IOrganizationFacadeQuery
    {
        private readonly IOrganizationQuery _organizationQuery;

        public OrganizationFacadeQuery(IOrganizationQuery organizationQuery)
        {
            _organizationQuery = organizationQuery;
        }

        public OrganizationViewModel GetOrganization(long id)
        {
            var organization = _organizationQuery.GetOrganization(id);
            return OrganizationMappers.Map(organization);
        }

        public List<OrganizationViewModel> GetOrganizations()
        {
            var organizations = _organizationQuery.GetOrganizations();
            return OrganizationMappers.Map(organizations);
        }
    }
}