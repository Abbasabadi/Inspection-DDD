﻿using Framework.Domain;

namespace Organization.Domain.Models.Personel
{
    public class Person: EntityBase<PersonId>
    {
        public string Name { get; private set; }
        public string Family { get; private set; }
        public string FatherName { get; private set; }
        public NationalCode NationalCode { get; private set; }
        public string EducationDegree { get; private set; }
        public string StudyField { get; private set; }
        public PhoneNumber PhoneNumber { get; private set; }
        public Email Email { get; private set; }
        public string Position { get; private set; }
        public long OrganizationId { get; private set; }

        public Person(PersonId id,string name, string family, string fatherName, NationalCode nationalCode, string educationDegree, string studyField, PhoneNumber phoneNumber, Email email, string position, long organizationId): base(id)
        {
            Name = name;
            Family = family;
            FatherName = fatherName;
            NationalCode = nationalCode;
            EducationDegree = educationDegree;
            StudyField = studyField;
            PhoneNumber = phoneNumber;
            Email = email;
            Position = position;
            OrganizationId = organizationId;
        }
    }
}
