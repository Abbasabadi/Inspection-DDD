﻿using Framework.Domain;

namespace Organization.Domain.Models.Personel
{
    public class NationalCode : ValueObjectBase
    {
        public string Value { get; set; }

        public NationalCode(string value)
        {
            //TODO: regular expression for national code
            Value = value;
        }
    }
}