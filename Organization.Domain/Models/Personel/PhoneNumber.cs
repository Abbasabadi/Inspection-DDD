﻿using Framework.Domain;

namespace Organization.Domain.Models.Personel
{
    public class PhoneNumber : ValueObjectBase
    {
        public string Value { get; set; }

        public PhoneNumber(string value)
        {
            //TODO: regular expression for phonen number
            Value = value;
        }
    }
}