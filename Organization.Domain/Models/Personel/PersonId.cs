﻿using Framework.Domain;

namespace Organization.Domain.Models.Personel
{
    public class PersonId : IdBase<long>
    {
        protected PersonId(long idDbId) : base(idDbId)
        {
        }
    }
}