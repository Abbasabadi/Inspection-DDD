﻿using Framework.Domain;

namespace Organization.Domain.Models.Personel
{
    public class Email : ValueObjectBase
    {
        public string Value { get; set; }

        public Email(string value)
        {
            //TODO: regular expression for Email
            Value = value;
        }
    }
}