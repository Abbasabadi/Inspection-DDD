﻿using Framework.Domain;

namespace Organization.Domain.Models.Organizations
{
    public class OrganizationId : IdBase<long>
    {
        public OrganizationId(long idDbId) : base(idDbId)
        {
        }
    }
}