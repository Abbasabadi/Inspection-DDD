﻿using System;
using Framework.Domain;

namespace Organization.Domain.Models.Organizations
{
    public class Organization : EntityBase<OrganizationId>
    {
        public string Name { get; private set; }
        public bool IsHeadQuarter { get; private set; }


        public Organization(OrganizationId id, string name, bool isHeadQuarter): base(id)
        {
            GaurdAganistNullOrEmptyName(name);

            Name = name;
            IsHeadQuarter = isHeadQuarter;
            CreationDateTime = DateTime.Now;
        }

        private static void GaurdAganistNullOrEmptyName(string name)
        {
            if(string.IsNullOrEmpty(name))
                throw new ArgumentNullException();
        }
    }
}