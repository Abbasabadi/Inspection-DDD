﻿using System.Collections.Generic;

namespace Organization.Domain.Models.Organizations
{
    public interface IOrganizationRepository
    {
        void CreataOrganization(Organization organization);
        long GetNextId();
    }
}