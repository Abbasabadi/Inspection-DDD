﻿using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Castle.Windsor;

namespace ServiceHost
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var container = new WindsorContainer();
            Framework.Castle.Bootstrapper.WireUp(container);
            Organization.Infrastructure.Config.OrganizationBootstrapper.WireUp(container);
            BasicInformation.Infrastructure.Config.BasicInformationBootstrapper.WireUp(container);
            Inspection.Infrastructure.Config.InspectionBootstrapper.WireUp(container);
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator),
                new WindsorControllerActivator(container));
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}