﻿using Inspection.Infrastructure.Query.Mongo.Inspection;
using Inspection.Interface.Facade.Contracts.Inspection.Query;

namespace Inspection.Interface.Facade.Query
{
    public class InspectionFacadeQuery : IInspectionFacadeQuery
    {
        private readonly IInspectionQuery _inspectionQuery;

        public InspectionFacadeQuery(IInspectionQuery inspectionQuery)
        {
            _inspectionQuery = inspectionQuery;
        }

        public InspectionViewModel GetInspection(long inspectionId)
        {
            var inspection = _inspectionQuery.GetInspection(inspectionId);
            return Map(inspection);
        }

        public bool IsQuestionUsed(long questionId)
        {
            var inspections = _inspectionQuery.GetInspections();

            foreach (var inspection in inspections)
            {
                foreach (var questionAnswer in inspection.QuestionAnswers)
                {
                    if (questionAnswer.QuestionId == questionId)
                        return true;
                }
            }
            return false;
        }

        private static InspectionViewModel Map(Domain.Models.Inspection.Inspection inspection)
        {
            return new InspectionViewModel
            {
                Id = inspection.Id.DbId,
                InspectorName = inspection.InspectorName,
                StartDate = inspection.StartDate,
                EndDate = inspection.EndDate
            };
        }
    }
}