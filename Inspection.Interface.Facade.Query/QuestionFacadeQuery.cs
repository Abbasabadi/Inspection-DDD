﻿using System.Collections.Generic;
using Inspection.Domain.Models.Department;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.Question.Query;

namespace Inspection.Interface.Facade.Query
{
    public class QuestionFacadeQuery : IQuestionFacadeQuery
    {
        private readonly IQuestionQuery _questionQuery;
        private readonly IQuestionCategoryQuery _questionCategoryQuery;
        private readonly IDepartmentQuery _departmentQuery;

        public QuestionFacadeQuery(IQuestionQuery questionQuery, IQuestionCategoryQuery questionCategoryQuery,
            IDepartmentQuery departmentQuery)
        {
            _questionQuery = questionQuery;
            _questionCategoryQuery = questionCategoryQuery;
            _departmentQuery = departmentQuery;
        }

        public List<QuestionListViewModel> GetQuestions()
        {
            var result = new List<QuestionListViewModel>();
            var questions = _questionQuery.GetQuestions();
            var questionCategories = _questionCategoryQuery.GetQuestionCategories();
            var departments = _departmentQuery.GetDepartments();
            questions.ForEach(question =>
            {
                string questionCategoryName = null;
                string departmentName = null;
                questionCategories.ForEach(category =>
                {
                    if (question.QuestionCategoryId.Value == category.Id.DbId)
                        questionCategoryName = category.Name;

                    departments.ForEach(department =>
                    {
                        if (question.DepartmentId.Value == department.Id.DbId)
                            departmentName = department.Name;
                    });
                });

                result.Add(Map(question, questionCategoryName, departmentName));
            });
            return result;
        }

        public QuestionDetailViewModel GetQuestionBy(long questionId)
        {
            var question = _questionQuery.GetQuestionBy(questionId);
            var questionCategory = _questionCategoryQuery.GetQuestionCategoryBy(question.QuestionCategoryId.Value);
            var department = _departmentQuery.GetDepartmentBy(question.DepartmentId.Value);
            return Map(question, questionCategory, department);
        }

        private static QuestionListViewModel Map(Question question, string questionCategory, string department)
        {
            return new QuestionListViewModel
            {
                Id = question.Id.DbId,
                Label = question.Label,
                Key = question.Key,
                IsActive = question.IsActive,
                QuestionCategoryName = questionCategory,
                DepartmentName = department,
                ControllerType = question.ControllerType.Value
            };
        }

        private static QuestionDetailViewModel Map(Question question, QuestionCategory questionCategory,
            Department department)
        {
            return new QuestionDetailViewModel
            {
                Id = question.Id.DbId,
                Label = question.Label,
                Key = question.Key,
                IsActive = question.IsActive,
                QuestionCategoryName = questionCategory.Name,
                QuestionCategoryId = questionCategory.Id.DbId,
                DepartmentName = department.Name,
                DepartmentId = department.Id.DbId,
                ControllerType = question.ControllerType.Value
            };
        }
    }
}