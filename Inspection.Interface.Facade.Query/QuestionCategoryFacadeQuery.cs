﻿using System.Collections.Generic;
using System.Linq;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.Question.Query;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;

namespace Inspection.Interface.Facade.Query
{
    public class QuestionCategoryFacadeQuery : IQuestionCategoryFacadeQuery
    {
        private readonly IQuestionCategoryQuery _questionCategoryQuery;
        private readonly IDepartmentQuery _departmentQuery;
        private readonly IQuestionQuery _questionQuery;

        public QuestionCategoryFacadeQuery(IQuestionCategoryQuery questionCategoryQuery, IQuestionQuery questionQuery, IDepartmentQuery departmentQuery)
        {
            _questionCategoryQuery = questionCategoryQuery;
            _questionQuery = questionQuery;
            _departmentQuery = departmentQuery;
        }

        public List<QuestionCategoryViewModel> GetQuestionCategoriesWithQuestions(long departmentId)
        {
            var questionCategories = _questionCategoryQuery.GetQuestionCategoriesBy(departmentId);
            var mappedQuestionCategoires = Map(questionCategories);
                foreach (var questionCategory in mappedQuestionCategoires)
                {
                    var question = _questionQuery.GetQuestionsByQuestionCategoryId(questionCategory.Id);
                    var mappedQuestion = Map(question);
                    questionCategory.Questions.AddRange(mappedQuestion);
                }

            return mappedQuestionCategoires;
        }

        public List<QuestionCategoryViewModel> GetQuestionCategoriesBy(long departmentId)
        {
            var questionCategories = _questionCategoryQuery.GetQuestionCategoriesBy(departmentId);
            return Map(questionCategories);
        }

        public List<QuestionCategoryViewModel> GetQuestionCategories()
        {
            var departments = _departmentQuery.GetDepartments();
            var questionCategories = _questionCategoryQuery.GetQuestionCategories();
            var result = new List<QuestionCategoryViewModel>();
            foreach (var questionCategory in questionCategories)
            {
                foreach (var department in departments)
                {
                    if(questionCategory.DepartmentId.Value == department.Id.DbId)
                        result.Add(Map(questionCategory, department.Name));
                }
            }

            return result;
        }

        //TODO: Seperate And Unit Test Mappings
        private static QuestionCategoryViewModel Map(QuestionCategory questionCategory, string departmentName)
        {
            return new QuestionCategoryViewModel
            {
                Id = questionCategory.Id.DbId,
                Name = questionCategory.Name,
                DepartmentName = departmentName,
                IsActive = questionCategory.IsActive
            };
        }

        private static List<QuestionCategoryViewModel> Map(IEnumerable<QuestionCategory> questionCategories)
        {
            return questionCategories.Select(Map).ToList();
        }

        private static QuestionCategoryViewModel Map(QuestionCategory questionCategory)
        {
            return new QuestionCategoryViewModel
            {
                Id = questionCategory.Id.DbId,
                Name = questionCategory.Name,
                Questions = new List<QuestionViewModel>()
            };
        }

        private static IEnumerable<QuestionViewModel> Map(IEnumerable<Question> questions)
        {
            return questions.Select(Map).ToList();
        }

        private static QuestionViewModel Map(Question question)
        {
            return new QuestionViewModel
            {
                Id = question.Id.DbId,
                Key = question.Key,
                ControllerType = question.ControllerType.Value,
                IsRequired = question.IsRequired,
                Label = question.Label,
                Order = question.Order
            };
        }
    }
}