﻿using System.Collections.Generic;
using System.Linq;
using Inspection.Domain.Models.Department;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Interface.Facade.Contracts.Department.Query;

namespace Inspection.Interface.Facade.Query
{
    public class DepartmentFacadeQuery : IDepartmentFacadeQuery
    {
        private readonly IDepartmentQuery _departmentQuery;

        public DepartmentFacadeQuery(IDepartmentQuery departmentQuery)
        {
            _departmentQuery = departmentQuery;
        }

        public List<DepartmentViewModel> GetDepartments()
        {
            var departments = _departmentQuery.GetDepartments();
            return Map(departments);
        }

        private static List<DepartmentViewModel> Map(List<Department> departments)
        {
            return departments.Select(Map).ToList();
        }

        private static DepartmentViewModel Map(Department department)
        {
            return new DepartmentViewModel
            {
                Id = department.Id.DbId,
                Name = department.Name
            };
        }
    }
}