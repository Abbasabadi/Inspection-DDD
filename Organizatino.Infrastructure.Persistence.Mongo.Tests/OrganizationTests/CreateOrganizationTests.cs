﻿using System;
using Framework.MongoDb;
using Organization.Domain.Models.Organizations;
using Organization.Domain.Tests.Unit.Organization;
using Xunit;

namespace Organization.Infrastructure.Persistence.Mongo.Tests.OrganizationTests
{
    public class
        CreateOrganizationTests : RepositoryTestTearDown<Domain.Models.Organizations.Organization, OrganizationId>,
            IDisposable
    {
        private Domain.Models.Organizations.Organization _organization;
        private readonly OrganizationRepository _organizationRepository;

        public CreateOrganizationTests() : base("CSIS","Organizations")
        {
            _organizationRepository = new OrganizationRepository(Database);
        }

        [Fact]
        public void Should_Add_New_Organization_To_Database_When_Valid_Organization_Passed()
        {
            //Arrange
            var id = Database.GetNextSequenceFrom("OrganizationTestSeq");
            _organization = new OrganizationBuilder().WithId(id).Build();

            //Act
            _organizationRepository.CreataOrganization(_organization);
        }

        public void Dispose()
        {
            CleanUpInsertOperation(organization => organization.Id.DbId, _organization.Id.DbId);
        }
    }
}