﻿using System.Collections.Generic;
using System.Web.Http;
using Organization.Interface.Facade.Contract.Organizations.Command;
using Organization.Interface.Facade.Contract.Organizations.Query;

namespace Organization.Interface.Rest.Api
{
    public class OrganizationController : ApiController
    {
        private readonly IOrganizationFacadeCommand _organizationFacadeCommand;
        private readonly IOrganizationFacadeQuery _organizationFacadeQuery;

        public OrganizationController(IOrganizationFacadeCommand organizationFacadeCommand,
            IOrganizationFacadeQuery organizationFacadeQuery)
        {
            _organizationFacadeCommand = organizationFacadeCommand;
            _organizationFacadeQuery = organizationFacadeQuery;
        }

        [HttpPost]
        public void CreateOrganization(CreateOrganizationCommand command)
        {
            _organizationFacadeCommand.CreateOrganization(command);
        }

        [HttpGet]
        public OrganizationViewModel GetOrganization(long id)
        {
            return _organizationFacadeQuery.GetOrganization(id);
        }

        [HttpGet]
        public List<OrganizationViewModel> GetOrganizations()
        {
            return _organizationFacadeQuery.GetOrganizations();
        }
    }
}