﻿using Framework.Application.Command;
using Organization.Domain.Models.Organizations;
using Organization.Interface.Facade.Contract.Organizations.Command;

namespace Organization.Application
{
    public class OrganizationCommandHandler: ICommandHandler<CreateOrganizationCommand>
    {
        private readonly IOrganizationRepository _organizationRepository;
        public OrganizationCommandHandler(IOrganizationRepository organizationRepository)
        {
            _organizationRepository = organizationRepository;
        }

        public void Handle(CreateOrganizationCommand command)
        {
            var dbId = _organizationRepository.GetNextId();
            var organizationId = new OrganizationId(dbId);
            var organization = new Domain.Models.Organizations.Organization(organizationId,command.Name, command.IsHeadQuarter);
            _organizationRepository.CreataOrganization(organization);
        }
    }
}