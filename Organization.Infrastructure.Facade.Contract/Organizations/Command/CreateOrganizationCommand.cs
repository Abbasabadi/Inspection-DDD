﻿namespace Organization.Interface.Facade.Contract.Organizations.Command
{
    public class CreateOrganizationCommand : Framework.Application.Command.ICommand
    {
        public string Name { get; set; }
        public bool IsHeadQuarter { get; set; }
    }
}