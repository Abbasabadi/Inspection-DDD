﻿namespace Organization.Interface.Facade.Contract.Organizations.Command
{
    public interface IOrganizationFacadeCommand
    {
        void CreateOrganization(CreateOrganizationCommand command);

    }
}
