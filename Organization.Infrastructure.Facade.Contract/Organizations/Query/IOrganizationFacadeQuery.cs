﻿using System.Collections.Generic;

namespace Organization.Interface.Facade.Contract.Organizations.Query
{
    public interface IOrganizationFacadeQuery
    {
        OrganizationViewModel GetOrganization(long id);
        List<OrganizationViewModel> GetOrganizations();
    }
}
