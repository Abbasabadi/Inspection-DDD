﻿using Framework.Application.Query;

namespace Organization.Interface.Facade.Contract.Organizations.Query
{
    public class OrganizationViewModel: IQuery
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsHeadQuarter { get; set; }
    }
}
