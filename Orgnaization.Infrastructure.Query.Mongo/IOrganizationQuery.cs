﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orgnaization.Infrastructure.Query.Mongo
{
    public interface IOrganizationQuery
    {
        Organization.Domain.Models.Organizations.Organization GetOrganization(long organizationId);
        List<Organization.Domain.Models.Organizations.Organization> GetOrganizations();
    }
}
