﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace Orgnaization.Infrastructure.Query.Mongo
{
    public class OrganizationQuery: IOrganizationQuery
    {
        private readonly IMongoCollection<Organization.Domain.Models.Organizations.Organization>
            _organizationCollection;

        public OrganizationQuery(IMongoDatabase database)
        {
            _organizationCollection =
                database.GetCollection<Organization.Domain.Models.Organizations.Organization>("Organizations");
        }

        public Organization.Domain.Models.Organizations.Organization GetOrganization(long organizationId)
        {
            //TODO: Handle exceptions
            var filter =
                Builders<Organization.Domain.Models.Organizations.Organization>.Filter.Eq(
                    organization => organization.Id.DbId,
                    organizationId);
            return _organizationCollection.Find(filter).First();
        }

        public List<Organization.Domain.Models.Organizations.Organization> GetOrganizations()
        {
            var filter = Builders<Organization.Domain.Models.Organizations.Organization>.Filter.Empty;
            return _organizationCollection.Find(filter).ToList();
        }
    }
}