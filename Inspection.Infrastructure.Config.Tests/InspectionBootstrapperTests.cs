﻿using System;
using Castle.Windsor;
using Inspection.Domain.Models.Question;
using Inspection.Interface.Facade.Contracts.Question.Command;
using Inspection.Interface.Rest.Api;
using Inspection.Interface.Rest.Api.Controllers;
using Xunit;

namespace Inspection.Infrastructure.Config.Tests
{
    public class InspectionBootstrapperTests
    {
        private readonly IWindsorContainer _container;

        public InspectionBootstrapperTests()
        {
            _container = new WindsorContainer();
        }

        [Fact]
        public void Wire_Up_Should_Register_Interfaces_With_Their_Implementaitions()
        {
            //Arrange

            //Act
            InspectionBootstrapper.WireUp(_container);

            //Assert
            Assert.True(IsComponentRegistered(typeof(QuestionController)));
            Assert.True(IsComponentRegistered(typeof(IQuestionRepository)));
            Assert.True(IsComponentRegistered(typeof(IQuestionFacadeCommand)));
            Assert.False(IsComponentRegistered(typeof(ICloneable)));
        }

        private bool IsComponentRegistered(Type component)
        {
            return _container.Kernel.HasComponent(component);
        }
    }
}