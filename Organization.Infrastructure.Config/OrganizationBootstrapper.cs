﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Organization.Interface.Facade.Command;
using Organization.Interface.Facade.Contract.Organizations.Command;
using Organization.Interface.Facade.Contract.Organizations.Query;
using Organization.Interface.Facade.Query;
using Organization.Interface.Rest.Api;
using Framework.Application.Command;
using MongoDB.Driver;
using Organization.Application;
using Organization.Domain.Models.Organizations;
using Organization.Infrastructure.Persistence.Mongo;
using Organization.Infrastructure.Persistence.Mongo.Mapping;
using Orgnaization.Infrastructure.Query.Mongo;

namespace Organization.Infrastructure.Config
{
    public class OrganizationBootstrapper
    {
        public static void WireUp(IWindsorContainer container)
        {
            container.Register(Component.For<OrganizationController>().LifestylePerWebRequest());

            container.Register(Component.For<IOrganizationQuery>().ImplementedBy<OrganizationQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IOrganizationFacadeCommand>().ImplementedBy<OrganizationFacadeCommand>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IOrganizationFacadeQuery>().ImplementedBy<OrganizationFacadeQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IOrganizationRepository>().ImplementedBy<OrganizationRepository>()
                .LifestylePerWebRequest());

            container.Register(Classes.FromAssemblyContaining<OrganizationCommandHandler>()
                .BasedOn(typeof(ICommandHandler<>)).WithService.AllInterfaces().LifestyleTransient());

            container.Register(Component.For<IMongoDatabase>()
                .UsingFactoryMethod(a => new MongoClient().GetDatabase("CSIS"))
                .LifestyleSingleton());

            MongoMappings.MapIdBases();
        }
    }
}