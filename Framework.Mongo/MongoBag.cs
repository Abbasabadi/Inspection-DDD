﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Domain;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Framework.MongoDb
{
    public static class MongoBag
    {
        public static T GetEntityId<T>(IMongoDatabase mongoDatabase, string collection)
        {
            var target = mongoDatabase.GetCollection<T>(collection);
            var entities = target.Find(FilterDefinition<T>.Empty);
            return entities.First();
        }
    }
}
