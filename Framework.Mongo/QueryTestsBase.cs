﻿using MongoDB.Driver;

namespace Framework.MongoDb
{
    public class QueryTestsBase<T,TI>
    {
        public IMongoDatabase Database { get; set; }

        public QueryTestsBase(string databaseName)
        {
            Database = MongoDatabaseCreator.Database<T>(databaseName);
        }
    }
}
