﻿using Framework.Domain;
using MongoDB.Bson.Serialization;

namespace Framework.MongoDb
{
    public static class MappingHelper
    {
        public static void ConfigIdBases<T>()
        {
            BsonClassMap.RegisterClassMap<EntityBase<T>>(cm =>
            {
                cm.AutoMap();
                cm.MapProperty(x => x.CreationDateTime);
                cm.UnmapProperty(x => x.EventPublisher);
            });
        }
    }
}