﻿using System;
using System.Linq.Expressions;
using Framework.Domain;
using MongoDB.Driver;

namespace Framework.MongoDb
{
    public static class MongoBuilders<T>
    {
        public static FilterDefinition<T> Filter<TU>(Expression<Func<T, TU>> conditionField, TU conditionValue)
        {
            return Builders<T>.Filter.Eq(conditionField, conditionValue);
        }

        public static UpdateDefinition<T> Update<TUf>(Expression<Func<T, TUf>> updateField, TUf updateValue)
        {
            return Builders<T>.Update.Set(updateField, updateValue);
        }
    }
}