﻿using System;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace Framework.MongoDb
{
    public class FindAndReplaceTearDown<T, TI>
    {
        public IMongoDatabase Database;
        private readonly string _collection;

        public FindAndReplaceTearDown(string databaseName, string collection)
        {
            _collection = collection;
            Database = MongoDatabaseCreator.Database<TI>(databaseName);
        }

        public void CleanUpFindAndReplaceOperation<TU>(Expression<Func<T, TU>> conditionField, TU conditionValue,
            T initialObject)
        {
            var collection = CreateCollection(_collection);
            var filter = MongoBuilders<T>.Filter(conditionField, conditionValue);
            collection.FindOneAndReplace(filter, initialObject);
            Database = null;
        }

        private IMongoCollection<T> CreateCollection(string collectionName)
        {
            return Database.GetCollection<T>(collectionName);
        }

        //private static void RegisterClassMap()
        //{
        //    BsonClassMap.RegisterClassMap<EntityBase<TI>>(cm =>
        //    {
        //        cm.AutoMap();
        //        cm.MapProperty(x => x.CreationDateTime);
        //        cm.UnmapProperty(x => x.EventPublisher);
        //    });
        //}

        //public static void UnregisterClassMap()
        //{
        //    var classType = typeof(EntityBase<TI>);
        //    GetClassMaps().Remove(classType);
        //}

        //public static Dictionary<Type, BsonClassMap> GetClassMaps()
        //{
        //    var bsonClassMaps = BsonClassMap.GetRegisteredClassMaps();
        //    var fi = typeof(BsonClassMap).GetField("__classMaps", BindingFlags.Static | BindingFlags.NonPublic);
        //    var classMaps = (Dictionary<Type, BsonClassMap>) fi.GetValue(bsonClassMaps);
        //    return classMaps;
        //}

        //public static void ClearClassMap()
        //{
        //    GetClassMaps().Clear();
        //}
    }
}