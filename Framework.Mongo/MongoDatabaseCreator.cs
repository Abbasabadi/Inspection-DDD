﻿using System;
using Framework.Domain;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace Framework.MongoDb
{
    public static class MongoDatabaseCreator
    {
        //Single tone it
        public static IMongoDatabase Database<T>(string collection)
        {
            RegisterClassMappings<T>();
            return new MongoClient().GetDatabase(collection);
        }

        private static void RegisterClassMappings<T>()
        {
            try
            {
                if (BsonClassMap.IsClassMapRegistered(typeof(T)))
                    return;
                BsonClassMap.RegisterClassMap<EntityBase<T>>(cm =>
                {
                    cm.AutoMap();
                    cm.MapProperty(x => x.CreationDateTime);
                    cm.UnmapProperty(x => x.EventPublisher);
                });
            }
            catch
            {
                RegisterClassMappings<T>();
            }
        }
    }
}