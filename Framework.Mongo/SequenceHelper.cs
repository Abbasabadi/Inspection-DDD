﻿using MongoDB.Driver;

namespace Framework.MongoDb
{
    public static class SequenceHelper
    {
        public static long GetNextSequenceFrom(this IMongoDatabase database, string sequenceName)
        {
            var collection = database.GetCollection<SequenceDocumentModel>("Sequences");
            var filter = Builders<SequenceDocumentModel>.Filter.Eq(a => a.Name, sequenceName);
            var update = Builders<SequenceDocumentModel>.Update.Inc(a => a.Value, 1);
            var document = collection.FindOneAndUpdate(filter, update);
            return document.Value;
        }
    }
}