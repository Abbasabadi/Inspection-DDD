﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace Framework.MongoDb
{
    public class RepositoryTestTearDown<T,TI>
    {
        public IMongoDatabase Database;
        private readonly string _collection;

        public RepositoryTestTearDown(string databaseName, string collection)
        {
            _collection = collection;
            Database = MongoDatabaseCreator.Database<TI>(databaseName);
        }

        public void CleanUpInsertOperation<TU>(Expression<Func<T, TU>> conditionField,
            TU conditionValue)
        {
            var collection = CreateCollection(_collection);
            var filter = MongoBuilders<T>.Filter(conditionField, conditionValue);
            collection.DeleteOne(filter);
            Database = null;
        }

        public void CleanUpUpdateOperation<TU, TUf>(Expression<Func<T, TU>> conditionField,
            TU conditionValue, Expression<Func<T, TUf>> updateField, TUf updateValue)
        {
            var collection = CreateCollection(_collection);
            var filter = MongoBuilders<T>.Filter(conditionField, conditionValue);
            var update = MongoBuilders<T>.Update(updateField, updateValue);
            collection.FindOneAndUpdate(filter, update);
            Database = null;
        }

        private IMongoCollection<T> CreateCollection(string collectionName)
        {
            return Database.GetCollection<T>(collectionName);
        }
    }
}