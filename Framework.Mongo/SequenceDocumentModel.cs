﻿namespace Framework.MongoDb
{
    public class SequenceDocumentModel
    {
        public object Id { get; set; }
        public long Value { get; set; }
        public string Name { get; set; }
    }
}