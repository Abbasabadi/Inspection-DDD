﻿using Framework.Application.Command;
using Moq;
using Organization.Interface.Facad.Command.Tests.Unit.CommandBuilders;
using Organization.Interface.Facade.Command;
using Xunit;

namespace Organization.Interface.Facad.Command.Tests.Unit
{
    public class OrganizationFacadeCommandTests
    {
        [Fact]
        public void Create_Organization_Should_Call_Dispatch_On_Command_Bus_When_Create_Organization_Command_Passed()
        {
            //Arrange
            //var commandBus = Substitute.For<ICommandBus>();
            var commandBus = new Mock<ICommandBus>();
            var facade = new OrganizationFacadeCommand(commandBus.Object);
            var command = CreateOrganizationCommandBuilder.Single();

            //Act
            facade.CreateOrganization(command);

            //Assert
            commandBus.Verify(x=>x.Dispatch(command));
        }
    }
}
