﻿using FizzWare.NBuilder;
using Organization.Interface.Facade.Contract.Organizations.Command;

namespace Organization.Interface.Facad.Command.Tests.Unit.CommandBuilders
{
    public static class CreateOrganizationCommandBuilder
    {
        public static CreateOrganizationCommand Single()
        {
            return Builder<CreateOrganizationCommand>.CreateNew()
                .With(x => x.Name, "وزارت خارجه")
                .With(x => x.IsHeadQuarter, true)
                .Build();
        }
    }
}