﻿using Framework.Application.Command;
using Inspection.Domain.Models.Department;
using Inspection.Interface.Facade.Contracts.Department.Command;

namespace Inspection.Application
{
    public class DepartmentCommandHandler : ICommandHandler<CreateDepartmentCommand>
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentCommandHandler(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        public void Handle(CreateDepartmentCommand command)
        {
            var id = _departmentRepository.GetNextId();
            var departmentId = new DepartmentId(id);
            var department = new Department(departmentId, command.Name);
            _departmentRepository.CreateDepartment(department);
        }
    }
}