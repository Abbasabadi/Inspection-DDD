﻿using Framework.Application.Command;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Models.Shared;
using Inspection.Interface.Facade.Contracts.Question.Command;

namespace Inspection.Application
{
    public class QuestionCommandHandler : ICommandHandler<CreateQuestionCommand>,
        ICommandHandler<ChangeQuestionActivationCommand>, ICommandHandler<UpdateQuestionCommand>
    {
        private readonly IQuestionRepository _questionRepository;

        public QuestionCommandHandler(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        public void Handle(CreateQuestionCommand command)
        {
            var id = _questionRepository.GetNextId();
            var questionId = new QuestionId(id);
            var question = new Question(questionId, command.Key, command.Label, command.IsRequired, command.Order, true,
                command.DepartmentId, command.QuestionCategoryId, command.ControllerType);
            _questionRepository.CreateQuestion(question);
        }

        public void Handle(ChangeQuestionActivationCommand command)
        {
            var question = _questionRepository.GetBy(command.Id);
            question.ChangeQuestionActivation(command.IsActive);
            _questionRepository.UpdateQuesiton(question);
        }

        public void Handle(UpdateQuestionCommand command)
        {
            var question = _questionRepository.GetBy(command.Id);
            question.Modify(command.DepartmentId, command.QuestionCategoryId, command.ControllerType, command.Label,
                command.Key, command.IsRequired);
            _questionRepository.UpdateQuesiton(question);
        }
    }
}