﻿using System.Linq;
using System.Collections.Generic;
using Framework.Application.Command;
using Framework.Core.Events;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Models.Shared;
using Inspection.Interface.Facade.Contracts.Inspection.Command;

namespace Inspection.Application
{
    public class InspectionCommandHandler : ICommandHandler<List<PlaceQuestionAnswerCommand>>,
        ICommandHandler<CreateInspectionCommand>
    {
        private readonly IInspectionRepository _inspectionRepository;
        private readonly IEventPublisher _eventPublisher;
        public InspectionCommandHandler(IInspectionRepository inspectionRepository, IEventPublisher eventPublisher)
        {
            _inspectionRepository = inspectionRepository;
            _eventPublisher = eventPublisher;
        }

        public void Handle(CreateInspectionCommand command)
        {
            var id = _inspectionRepository.GetNextId();
            var inspectionId = new InspectionId(id);
            var organizationId = new OrganizationId(command.OrganizationId);
            var departmentId = new DepartmentId(command.DepartmentId);
            var inspection = new Domain.Models.Inspection.Inspection(inspectionId, command.InspectorName,
                command.StartDate, command.EndDate, organizationId, departmentId, _eventPublisher);
            _inspectionRepository.CreateInspection(inspection);
        }

        public void Handle(List<PlaceQuestionAnswerCommand> commands)
        {
            var inspectionId = commands.First().InspectionId;
            var inspection = _inspectionRepository.GetById(inspectionId);
            var questionAnswers = Map(commands);
            inspection.PlaceQuestionAnswers(questionAnswers);
            _inspectionRepository.UpdateInspectionAnswers(inspection);
        }

        public List<QuestionAnswer> Map(List<PlaceQuestionAnswerCommand> commands)
        {
            var result = new List<QuestionAnswer>();
            foreach (var command in commands)
            {
                var questionAnswer = new QuestionAnswer(command.Answer, command.QuestionId);
                result.Add(questionAnswer);
            }

            return result;
        }
    }
}