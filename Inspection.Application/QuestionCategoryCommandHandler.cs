﻿using Framework.Application.Command;
using Framework.Core.Events;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Domain.Models.Shared;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;

namespace Inspection.Application
{
    public class QuestionCategoryCommandHandler : ICommandHandler<CreateQuestionCategoryCommand>, ICommandHandler<ChangeQuestionCategoryActivationCommand>
    {
        private readonly IQuestionCategoryRepository _questionCategoryRepository;
        private readonly IEventPublisher _eventPublisher;

        public QuestionCategoryCommandHandler(IQuestionCategoryRepository questionCategoryRepository, IEventPublisher eventPublisher)
        {
            _questionCategoryRepository = questionCategoryRepository;
            _eventPublisher = eventPublisher;
        }

        public void Handle(CreateQuestionCategoryCommand command)
        {
            var id = _questionCategoryRepository.GetNextId();
            var qeustionCategoryid = new QuestionCategoryId(id);
            var departmentId = new DepartmentId(command.DepartmentId);
            var questionCategory = new QuestionCategory(qeustionCategoryid, command.Name, departmentId);
            _questionCategoryRepository.Create(questionCategory);
        }

        public void Handle(ChangeQuestionCategoryActivationCommand command)
        {
            var questionCategory = _questionCategoryRepository.GetBy(command.Id);
            questionCategory.ChangeActivationStatus(command.IsActive);
            _questionCategoryRepository.Update(questionCategory);
        }
    }
}