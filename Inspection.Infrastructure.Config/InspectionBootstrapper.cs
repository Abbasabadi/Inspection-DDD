﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Framework.Application.Command;
using Framework.Core.Events;
using Inspection.Application;
using Inspection.Domain.Models.Department;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo.Mapping;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.Inspection;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Command;
using Inspection.Interface.Facade.Contracts.Department.Command;
using Inspection.Interface.Facade.Contracts.Department.Query;
using Inspection.Interface.Facade.Contracts.Inspection.Command;
using Inspection.Interface.Facade.Contracts.Inspection.Query;
using Inspection.Interface.Facade.Contracts.Question.Command;
using Inspection.Interface.Facade.Contracts.Question.Query;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;
using Inspection.Interface.Facade.Query;
using Inspection.Interface.Rest.Api.Controllers;

namespace Inspection.Infrastructure.Config
{
    public static class InspectionBootstrapper
    {
        public static void WireUp(IWindsorContainer container)
        {
            container.Register(Component.For<QuestionController>().LifestylePerWebRequest());

            container.Register(Component.For<DepartmentController>().LifestylePerWebRequest());

            container.Register(Component.For<InspectionController>().LifestylePerWebRequest());

            container.Register(Component.For<QuestionCategoryController>().LifestylePerWebRequest());

            container.Register(Component.For<IQuestionFacadeCommand>().ImplementedBy<QuestionFacadeCommand>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IDepartmentFacadeCommand>().ImplementedBy<DepartmentFacadeCommand>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionCategoryFacadeCommand>()
                .ImplementedBy<QuestionCategoryFacadeCommand>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IInspectionFacadeCommand>()
                .ImplementedBy<InspectionFacadeCommand>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionCategoryFacadeQuery>()
                .ImplementedBy<QuestionCategoryFacadeQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IInspectionFacadeQuery>().ImplementedBy<InspectionFacadeQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionFacadeQuery>()
                .ImplementedBy<QuestionFacadeQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IDepartmentFacadeQuery>()
                .ImplementedBy<DepartmentFacadeQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionCategoryQuery>()
                .ImplementedBy<QuestionCategoryQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionQuery>()
                .ImplementedBy<QuestionQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IDepartmentQuery>()
                .ImplementedBy<DepartmentQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IInspectionQuery>().ImplementedBy<InspectionQuery>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionRepository>().ImplementedBy<QuestionRepository>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IDepartmentRepository>().ImplementedBy<DepartmentRepository>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IQuestionCategoryRepository>().ImplementedBy<QuestionCategoryRepository>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IInspectionRepository>().ImplementedBy<InspectionRepository>()
                .LifestylePerWebRequest());

            container.Register(Component.For<IEventListener>().Forward<IEventPublisher>()
                .ImplementedBy<EventAggregator>().LifestylePerWebRequest());
            container.Register(Classes.FromAssemblyContaining<QuestionCommandHandler>()
                .BasedOn(typeof(ICommandHandler<>)).WithService.AllInterfaces().LifestyleTransient());

            MongoMappings.MapIdBases();
        }
    }
}