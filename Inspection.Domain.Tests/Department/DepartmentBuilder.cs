﻿using Framework.Core;
using Inspection.Domain.Models.Department;

namespace Inspection.Domain.Tests.Unit.Department
{
    public class DepartmentBuilder : IBuilder<Models.Department.Department>
    {
        public long Id;
        public string Name;
        public bool IsActive { get; set; }
        public DepartmentBuilder()
        {
            Id = 5;
            Name = "حراست IT";
            IsActive = true;
        }

        public DepartmentBuilder WithId(long id)
        {
            Id = id;
            return this;
        }

        public DepartmentBuilder WithName(string name)
        {
            Name = name;
            return this;
        }

        public DepartmentBuilder WithIsActive(bool isActive)
        {
            IsActive = isActive;
            return this;
        }

        public Models.Department.Department Build()
        {
            var departmentId = new DepartmentId(Id);
            return new Models.Department.Department(departmentId, Name);
        }

    }
}