﻿using System;
using Xunit;

namespace Inspection.Domain.Tests.Unit.Department
{
    public class DepartmentTests
    {
        private readonly DepartmentBuilder _departmentBuilder;

        public DepartmentTests()
        {
            _departmentBuilder = new DepartmentBuilder();
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void Cant_Create_Department_When_Department_Name_Is_Null_Or_Empty(string departmentName)
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _departmentBuilder.WithName(departmentName).Build();
            });
        }

        [Fact]
        public void Constructor_Should_Construct_Department_Properly()
        {
            //Act
            var department = _departmentBuilder.Build();

            //Assert
            Assert.Equal(_departmentBuilder.Id, department.Id.DbId);
            Assert.Equal(_departmentBuilder.Name, department.Name);
            Assert.Equal(_departmentBuilder.IsActive, department.IsActive);
        }


        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void ChangeActivation_Should_Update_Department_IsActive_When_IsActive_Is_Passed(bool isActive)
        {
            //Arrange
            var department = _departmentBuilder.Build();

            //Act
            department.ChangeActivation(isActive);

            //Assert
            Assert.Equal(isActive, department.IsActive);
        }
    }
}