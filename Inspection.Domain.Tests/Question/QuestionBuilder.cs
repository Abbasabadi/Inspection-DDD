﻿using Framework.Core;
using Inspection.Domain.Models.Question;

namespace Inspection.Domain.Tests.Unit.Question
{
    public class QuestionBuilder : IBuilder<Models.Question.Question>
    {
        public long Id;
        public string Value;
        public string Key;
        public string Label;
        public bool IsRequired;
        public int Order;
        public bool IsActive;
        public string ControllerType;
        public long DepartmentId;
        public long QuestionCategoryId;

        public QuestionBuilder()
        {
            Id = 16;
            Value = "مقدار تستی";
            Key = "مقدار تستی";
            Label = "مقدار تستی";
            IsRequired = true;
            Order = 2;
            IsActive = true;
            ControllerType = "CheckBox";
            DepartmentId = 10;
            QuestionCategoryId = 12;
        }

        public QuestionBuilder WithId(long id)
        {
            Id = id;
            return this;
        }

        public QuestionBuilder WithDepartmentId(long departmentId)
        {
            DepartmentId = departmentId;
            return this;
        }

        public QuestionBuilder WithQuestionCategoryId(long questionCategoryId)
        {
            QuestionCategoryId = questionCategoryId;
            return this;
        }

        public QuestionBuilder WithIsActive(bool status)
        {
            IsActive = status;
            return this;
        }

        public QuestionBuilder WithLabel(string label)
        {
            Label = label;
            return this;
        }

        public QuestionBuilder WithKey(string key)
        {
            Key = key;
            return this;
        }

        public QuestionBuilder WithIsRequired(bool isRequired)
        {
            IsRequired = isRequired;
            return this;
        }
        public QuestionBuilder WithControllerType(string controllerType)
        {
            ControllerType = controllerType;
            return this;
        }

        public Models.Question.Question Build()
        {
            var id = new QuestionId(Id);
            return new Models.Question.Question(id, Key, Label, IsRequired, Order, IsActive, DepartmentId,
                QuestionCategoryId, ControllerType);
        }
    }
}