﻿using Xunit;

namespace Inspection.Domain.Tests.Unit.Question
{
    public class QuestionTests
    {
        private readonly QuestionBuilder _questionBuilder;

        public QuestionTests()
        {
            _questionBuilder = new QuestionBuilder();
        }

        [Fact]
        public void Constructor_Should_Construct_Question_Properly()
        {
            //Act
            var question = _questionBuilder.Build();

            //Assert
            Assert.Equal(_questionBuilder.Id, question.Id.DbId);
            Assert.Equal(_questionBuilder.Key, question.Key);
            Assert.Equal(_questionBuilder.Label, question.Label);
            Assert.Equal(_questionBuilder.IsRequired, question.IsRequired);
            Assert.Equal(_questionBuilder.Order, question.Order);
            Assert.Equal(_questionBuilder.IsActive, question.IsActive);
            Assert.Equal(_questionBuilder.QuestionCategoryId, question.QuestionCategoryId.Value);
            Assert.Equal(_questionBuilder.DepartmentId, question.DepartmentId.Value);
            Assert.Equal(_questionBuilder.ControllerType, question.ControllerType.Value);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void ChangeQuestionActivation_Should_Change_Is_Active_Status_When_Question_Is_Active_Passed(
            bool isActive)
        {
            //Arrange
            var question = _questionBuilder.WithIsActive(isActive).Build();

            //Act
            question.ChangeQuestionActivation(isActive);

            //Assert
            Assert.Equal(isActive, question.IsActive);
        }


        [Fact]
        public void Modify_Should_Update_Question_Properties_When_New_Values_Passed()
        {
            //Arrange
            var expectedQuestion = CreateExpectedQuestion();
            var question = _questionBuilder.Build();

            //Act
            question.Modify(expectedQuestion.DepartmentId, expectedQuestion.QuestionCategoryId, expectedQuestion.ControllerType, expectedQuestion.Label, expectedQuestion.Key, expectedQuestion.IsRequired);

            //Assert
            Assert.Equal(expectedQuestion.DepartmentId, question.DepartmentId.Value);
            Assert.Equal(expectedQuestion.QuestionCategoryId, question.QuestionCategoryId.Value);
            Assert.Equal(expectedQuestion.ControllerType, question.ControllerType.Value);
            Assert.Equal(expectedQuestion.Label, question.Label);
            Assert.Equal(expectedQuestion.Key, question.Key);
            Assert.Equal(expectedQuestion.IsRequired, question.IsRequired);
        }

        private QuestionBuilder CreateExpectedQuestion()
        {
            return _questionBuilder.WithLabel("newLabel").WithKey("newKey").WithIsRequired(false).WithQuestionCategoryId(10).WithDepartmentId(1).WithControllerType("checkBox");
        }
    }
}