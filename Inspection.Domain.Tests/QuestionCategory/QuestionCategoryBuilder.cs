﻿using Framework.Core;
using Framework.Core.Events;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Domain.Models.Shared;

namespace Inspection.Domain.Tests.Unit.QuestionCategory
{
    public class QuestionCategoryBuilder : IBuilder<Models.QuestionCategory.QuestionCategory>
    {
        public long Id;
        public string Name;
        public bool IsActive;
        public long DepartmentId;
        public IEventPublisher Publisher;

        public QuestionCategoryBuilder()
        {
            Id = 2;
            Name = "اتاق سرور";
            IsActive = true;
            DepartmentId = 1;
        }

        public QuestionCategoryBuilder WithId(long id)
        {
            Id = id;
            return this;
        }

        public QuestionCategoryBuilder WithName(string name)
        {
            Name = name;
            return this;
        }

        public QuestionCategoryBuilder WithDepartmentId(long departmentId)
        {
            DepartmentId = departmentId;
            return this;
        }

        public QuestionCategoryBuilder WithIsActive(bool isActive)
        {
            IsActive = isActive;
            return this;
        }

        public QuestionCategoryBuilder WithPublisher(IEventPublisher publisher)
        {
            Publisher = publisher;
            return this;
        }

        public Models.QuestionCategory.QuestionCategory Build()
        {
            var questionCategoryId = new QuestionCategoryId(Id);
            var departmentId = new DepartmentId(DepartmentId);
            return new Models.QuestionCategory.QuestionCategory(questionCategoryId, Name, departmentId);
        }
    }
}