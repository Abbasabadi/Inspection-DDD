﻿using System;
using Moq;
using Xunit;

namespace Inspection.Domain.Tests.Unit.QuestionCategory
{
    public class QuestionCategoryTests
    {
        private readonly QuestionCategoryBuilder _questionCategoryBuilder;

        public QuestionCategoryTests()
        {
            _questionCategoryBuilder = new QuestionCategoryBuilder();
        }
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Cant_Create_Question_Category_When_Question_Category_Name_Is_Null_Or_Empty(
            string questionCategoryName)
        {
            Assert.Throws<ArgumentNullException>(() =>
                _questionCategoryBuilder.WithName(questionCategoryName).Build());
        }

        [Fact]
        public void Constructor_Should_Construct_QuestionCategory_Properly()
        {
            //Arragne

            //Act
            var questionCategory = _questionCategoryBuilder.Build();

            //Assert
            Assert.Equal(_questionCategoryBuilder.Id, questionCategory.Id.DbId);
            Assert.Equal(_questionCategoryBuilder.Name, questionCategory.Name);
            Assert.Equal(_questionCategoryBuilder.IsActive, questionCategory.IsActive);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void Change_Question_Category_Activation_Should_Chagne_Is_Active_Status_When_Is_Active_Passed(
            bool isActive)
        {
            //Arrange
            var questionCategory = _questionCategoryBuilder.WithIsActive(isActive).Build();

            //Act
            questionCategory.ChangeActivationStatus(isActive);

            //Assert
            Assert.Equal(isActive, questionCategory.IsActive);
        }

        [Fact]
        public void Modify_Should_Update_Question_Category_Properties_When_New_Values_Passed()
        {
            //Arrange
            var expectedQuestionCategory = _questionCategoryBuilder.WithName("name").WithDepartmentId(39);
            var questionCategory = _questionCategoryBuilder.Build();

            //Act
            questionCategory.Modify(expectedQuestionCategory.Name, expectedQuestionCategory.DepartmentId);

            //Assert
            Assert.Equal(expectedQuestionCategory.Name, questionCategory.Name);
            Assert.Equal(expectedQuestionCategory.DepartmentId, questionCategory.DepartmentId.Value);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Modify_Should_Throw_When_Null_Or_Empty_Name_Passed(string name)
        {
            //Arrange
            var questionCategory = _questionCategoryBuilder.Build();

            //Assert
            Assert.Throws<ArgumentNullException>(() =>
                questionCategory.Modify(name, It.IsAny<long>()));
        }
    }
}