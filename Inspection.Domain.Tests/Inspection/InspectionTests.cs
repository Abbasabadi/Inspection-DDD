﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Core.Events;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Models.Inspection.Exceptions;
using Moq;
using Xunit;

namespace Inspection.Domain.Tests.Unit.Inspection
{
    public class InspectionTests
    {
        private readonly InspectionBuilder _inspectionBuilder;

        public InspectionTests()
        {
            _inspectionBuilder = new InspectionBuilder();
        }

        [Fact]
        public void Create_Inspection_When_Id_Is_Passed()
        {
            //Arrange
            const long inspectionId = 3;

            //Act
            var inspection = _inspectionBuilder.WithId(inspectionId).WithEventPublisher(new FakePublisher()).Build();

            //Assert
            Assert.NotNull(inspection.InspectorName);
            Assert.Equal(inspectionId, inspection.Id.DbId);
        }

        [Fact]
        public void Inpsection_Constructor_Should_Call_Publish_On_IEventPublisher()
        {
            //Arrange
            const long inspectionId = 3;
            var eventPublisher = new Mock<IEventPublisher>();

            //Act
            _inspectionBuilder.WithId(inspectionId).WithEventPublisher(eventPublisher.Object).Build();

            //Assert
            eventPublisher.Verify(x => x.Publish(It.IsAny<IEvent>()));
        }

        [Fact]
        public void Cant_Create_Inspection_When_Start_Date_Is_Bigger_Than_Today()
        {
            Assert.Throws<StartDateBiggerThanTodayException>(() =>
                _inspectionBuilder.WithStartDate(DateTime.Today.AddDays(2)).Build()
            );
        }

        [Fact]
        public void Cant_Create_Inspection_When_Start_Date_Is_Bigger_Than_EndDate()
        {
            Assert.Throws<StartDateIsBiggerThanEndDateException>(() =>
                _inspectionBuilder.WithStartDate(DateTime.Today.AddDays(3)).WithEndDate(DateTime.Today.AddDays(1))
                    .Build()
            );
        }

        [Fact]
        public void
            Place_Question_Answers_Should_Update_Answers_In_Question_Answers_List_When_Question_Answers_Are_Passed_And_They_Are_Duplicated()
        {
            //Arrange
            var questionAnswers = OriginalQuestionAnswers();

            var inspection = _inspectionBuilder.WithQuestionAnswers(questionAnswers).Build();

            //Act
            inspection.PlaceQuestionAnswers(questionAnswers);

            //Assert
            Assert.Equal(questionAnswers.First().Answer, inspection.QuestionAnswers.First().Answer);
        }

        [Fact]
        public void
            Place_Question_Answers_Should_Add_Answers_In_Question_Answers_List_When_Question_Answers_Are_Passed_And_They_Are_Not_Duplicated()
        {
            //Arrange
            var originalQuestionAnswers = OriginalQuestionAnswers();
            var passingQuestionAnswers = PassingQuestionAnswers();
            var inspection = _inspectionBuilder.WithQuestionAnswers(originalQuestionAnswers).Build();

            //Act
            inspection.PlaceQuestionAnswers(passingQuestionAnswers);
            var expectedQuestionAnswers = originalQuestionAnswers.Count + passingQuestionAnswers.Count;

            //Assert
            Assert.Equal(expectedQuestionAnswers, inspection.QuestionAnswers.Count);
        }

        private static List<QuestionAnswer> PassingQuestionAnswers()
        {
            return new List<QuestionAnswer>
            {
                new QuestionAnswer("changedTest1", 9),
                new QuestionAnswer("changedTest2", 10),
                new QuestionAnswer("changedTest3", 11)
            };
        }

        private static List<QuestionAnswer> OriginalQuestionAnswers()
        {
            var questionAnswers = new List<QuestionAnswer>
            {
                new QuestionAnswer("test1", 6),
                new QuestionAnswer("test2", 7),
                new QuestionAnswer("test3", 8)
            };
            return questionAnswers;
        }
    }
}