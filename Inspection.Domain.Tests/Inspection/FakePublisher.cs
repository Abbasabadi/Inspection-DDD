﻿using Framework.Core.Events;

namespace Inspection.Domain.Tests.Unit.Inspection
{
    public class FakePublisher: IEventPublisher
    {
        public void Publish<T>(T @event) where T : IEvent
        {
        }
    }
}
