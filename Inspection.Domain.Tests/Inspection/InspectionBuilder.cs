﻿using System;
using System.Collections.Generic;
using Framework.Core;
using Framework.Core.Events;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Models.Shared;

namespace Inspection.Domain.Tests.Unit.Inspection
{
    public class InspectionBuilder : IBuilder<Models.Inspection.Inspection>
    {
        public long Id;
        public string InspectorName;
        public DateTime StartDate;
        public DateTime EndDate;
        public long OrganizationId;
        public long DepartmentId;
        public bool IsFinal;
        public List<QuestionAnswer> QuestionAnswers;
        public IEventPublisher Publisher;

        public InspectionBuilder()
        {
            Id = 12;
            InspectorName = "حسین عباس آبادی";
            StartDate = DateTime.Today;
            EndDate = DateTime.Today.AddDays(5);
            OrganizationId = 3;
            DepartmentId = 2;
            IsFinal = true;
            QuestionAnswers = new List<QuestionAnswer>();
            Publisher = new FakePublisher();
        }


        public InspectionBuilder WithId(long id)
        {
            Id = id;
            return this;
        }

        public InspectionBuilder WithInspectorName(string inspectorName)
        {
            InspectorName = inspectorName;
            return this;
        }

        public InspectionBuilder WithStartDate(DateTime startDate)
        {
            StartDate = startDate;
            return this;
        }

        public InspectionBuilder WithEndDate(DateTime endDate)
        {
            EndDate = endDate;
            return this;
        }

        public InspectionBuilder WithOrganizatinId(long organizationId)
        {
            OrganizationId = organizationId;
            return this;
        }

        public InspectionBuilder WithDepartmentName(long departmentId)
        {
            DepartmentId = departmentId;
            return this;
        }

        public InspectionBuilder WithIsFinal(bool isFinal)
        {
            IsFinal = isFinal;
            return this;
        }

        public InspectionBuilder WithQuestionAnswers(List<QuestionAnswer> questionAnswers)
        {
            QuestionAnswers = questionAnswers;
            return this;
        }

        public InspectionBuilder WithEventPublisher(IEventPublisher publisher)
        {
            this.Publisher = publisher;
            return this;
        }

        public Models.Inspection.Inspection Build()
        {
            var id = new InspectionId(Id);
            var departmentId = new DepartmentId(DepartmentId);
            var organizationId = new OrganizationId(OrganizationId);
            var inspection = new Models.Inspection.Inspection(id, InspectorName, StartDate, EndDate, organizationId,
                departmentId, Publisher);
            inspection.PlaceQuestionAnswers(QuestionAnswers);
            return inspection;
        }
    }
}