﻿using Inspection.Domain.Models.Question;
using Inspection.Domain.Tests.Unit.Question;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Application.Tests.Unit.Question
{
    public class HandleChangeQuestionActivationCommandTests
    {
        [Fact]
        public void
            Should_Call_GetBy_And_ChangeQuestionActivation_On_QustionRepostitory_When_ChangeQuestionActivationCommand_Passed()
        {
            //Arragne
            var questionRepository = new Mock<IQuestionRepository>();
            var questionCommandHandler = new QuestionCommandHandler(questionRepository.Object);
            var command = ChangeQuestionActivationCommandBuilder.Single();
            var question = new QuestionBuilder().WithId(command.Id).WithIsActive(command.IsActive).Build();
            questionRepository.Setup(x => x.GetBy(command.Id)).Returns(question);

            //Act
            questionCommandHandler.Handle(command);

            //Assert
            questionRepository.Verify(x => x.GetBy(command.Id));
            questionRepository.Verify(x => x.UpdateQuesiton(question));
        }
    }
}