﻿using Inspection.Domain.Models.Question;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Application.Tests.Unit.Question
{
    public class HandleCreateQuestionCommandTests
    {
        [Fact]
        public void
            Should_Call_GetNextId_And_CreateQuestion_On_Question_Repository_When_Create_Question_Command_Passed()
        {
            //Arrange
            var questionRepository = new Mock<IQuestionRepository>();
            var questionCommandHandler = new QuestionCommandHandler(questionRepository.Object);
            var command = CreateQuestionCommandBuilder.Single();

            //Act
            questionCommandHandler.Handle(command);

            //Assert
            questionRepository.Verify(x => x.GetNextId());
            questionRepository.Verify(x => x.CreateQuestion(It.IsAny<Domain.Models.Question.Question>()));
        }
    }
}