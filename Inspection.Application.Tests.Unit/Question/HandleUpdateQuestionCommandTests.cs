﻿using Inspection.Domain.Models.Question;
using Inspection.Domain.Tests.Unit.Question;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Application.Tests.Unit.Question
{
    public class HandleUpdateQuestionCommandTests
    {
        [Fact]
        public void Should_Call_GetBy_And_UpdateQuestion_On_Question_Repository()
        {
            //Arrange
            var questionRepository = new Mock<IQuestionRepository>();
            var questionCommandHandler = new QuestionCommandHandler(questionRepository.Object);
            var command = UpdateQuestionCommandBuilder.Single();
            var question = new QuestionBuilder().WithId(command.Id).WithKey(command.Key).WithLabel(command.Label).WithIsRequired(command.IsRequired).WithDepartmentId(command.DepartmentId).WithQuestionCategoryId(command.QuestionCategoryId).Build();
            questionRepository.Setup(x => x.GetBy(command.Id)).Returns(question);

            //Act
            questionCommandHandler.Handle(command);

            //Assert
            questionRepository.Verify(x=>x.GetBy(command.Id));
            questionRepository.Verify(x=>x.UpdateQuesiton(question));
        }
    }
}
