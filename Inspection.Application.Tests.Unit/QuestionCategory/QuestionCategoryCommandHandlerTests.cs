﻿using Inspection.Domain.Models.QuestionCategory;
using Inspection.Domain.Tests.Unit.Inspection;
using Inspection.Domain.Tests.Unit.QuestionCategory;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;
using Moq;
using Xunit;

namespace Inspection.Application.Tests.Unit.QuestionCategory
{
    public class QuestionCategoryCommandHandlerTests
    {
        private long _questionCategoryId;
        private readonly QuestionCategoryBuilder _questionCategoryBuilder;
        private readonly Mock<IQuestionCategoryRepository> _questionCategoryRepository;
        private readonly QuestionCategoryCommandHandler _questionCategoryCommandHandler;

        public QuestionCategoryCommandHandlerTests()
        {
            _questionCategoryBuilder = new QuestionCategoryBuilder();
            _questionCategoryRepository = new Mock<IQuestionCategoryRepository>();
            var fakePublisher = new FakePublisher();
            _questionCategoryCommandHandler = new QuestionCategoryCommandHandler(_questionCategoryRepository.Object, fakePublisher);
        }

        [Fact]
        public void Handle_Create_Question_Should_Call_GetNextId_And_CreateOrganization_On_Organization_Repository_When_Create_Organization_Command_Passed()
        {
            //Arrange
            var command = CreateQuestionCategoryCommandBuilder.Single();

            //Act
            _questionCategoryCommandHandler.Handle(command);

            //Assert
            _questionCategoryRepository.Verify(x => x.GetNextId());
            _questionCategoryRepository.Verify(x => x.Create(It.IsAny<Domain.Models.QuestionCategory.QuestionCategory>()));
        }

        [Fact]
        public void
            Handle_Update_Question_Category_Activation_Command_Should_Call_Update_And_GetBy_On_Question_Category_Repository()
        {
            //Arrange
            _questionCategoryId = 1;
            var command = new ChangeQuestionCategoryActivationCommand(_questionCategoryId, false);
            var questionCategory =
                _questionCategoryBuilder.WithId(command.Id).WithIsActive(command.IsActive).Build();
            _questionCategoryRepository.Setup(x => x.GetBy(_questionCategoryId)).Returns(questionCategory);

            //Act
            _questionCategoryCommandHandler.Handle(command);

            //Assert
            _questionCategoryRepository.Verify(x => x.GetBy(_questionCategoryId));
            _questionCategoryRepository.Verify(x => x.Update(questionCategory));
        }
    }
}