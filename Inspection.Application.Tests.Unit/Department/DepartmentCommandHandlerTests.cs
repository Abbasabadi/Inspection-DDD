﻿using Inspection.Domain.Models.Department;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Application.Tests.Unit.Department
{
    public class DepartmentCommandHandlerTests
    {
        private readonly Mock<IDepartmentRepository> _departmentRepository;
        private readonly DepartmentCommandHandler _departmentCommandHandler;

        public DepartmentCommandHandlerTests()
        {
            _departmentRepository = new Mock<IDepartmentRepository>();
            _departmentCommandHandler = new DepartmentCommandHandler(_departmentRepository.Object);
        }

        [Fact]
        public void
            Handle_Should_Call_GetNextId_And_CreateDepartment_On_Department_Repository_When_Command_Dispatched()
        {
            //Arrange
            var command = CreateDepartmentCommandBuilder.Single();

            //Act
            _departmentCommandHandler.Handle(command);

            //Assert
            _departmentRepository.Verify(x => x.GetNextId());
            _departmentRepository.Verify(x => x.CreateDepartment(It.IsAny<Domain.Models.Department.Department>()));
        }
    }
}
