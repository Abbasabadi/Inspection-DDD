﻿using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Tests.Unit.Inspection;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using NSubstitute;
using Xunit;

namespace Inspection.Application.Tests.Unit.Inspection
{
    public class HandleCreateInspectionCommandTests
    {
        [Fact]
        public void Should_Call_GetById_And_CreateInspection_When_Create_Inspection_Command_Passed()
        {
            //Arrange
            var inspectionRepository = Substitute.For<IInspectionRepository>();
            var fakePublisher = new FakePublisher();
            var inspectionCommandHandler = new InspectionCommandHandler(inspectionRepository, fakePublisher);
            var command = CreateInspectionCommandBuilder.Single();

            //Act
            inspectionCommandHandler.Handle(command);

            //Assert
            inspectionRepository.Received().GetNextId();
            inspectionRepository.Received().CreateInspection(Arg.Any<Domain.Models.Inspection.Inspection>());
        }
    }
}