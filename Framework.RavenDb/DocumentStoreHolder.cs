﻿using Raven.Client.Documents;
using System;
using System.Security.Cryptography.X509Certificates;

namespace Framework.RavenDb
{
    public static class DocumentStoreHolder
    {
        public static IDocumentStore CreateStore(string certificatePath, string url, string database)
        {
            var clientCertificate = new X509Certificate2(certificatePath);
            var store = new DocumentStore()
            {
                Urls = new[] {url},
                Database = database,
                Certificate = clientCertificate
            }.Initialize();
            return store;
        }
    }
}