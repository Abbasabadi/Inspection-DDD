﻿using Framework.Core;
using Raven.Client.Documents.Session;

namespace Framework.RavenDb
{
    public class RavenUnitOfWork : IUnitOfWork
    {
        private readonly IDocumentSession _session;

        public RavenUnitOfWork(IDocumentSession session)
        {
            _session = session;
        }

        public void Begin()
        {
        }

        public void Commit()
        {
            _session.SaveChanges();
        }

        public void Rollback()
        {
        }
    }
}
