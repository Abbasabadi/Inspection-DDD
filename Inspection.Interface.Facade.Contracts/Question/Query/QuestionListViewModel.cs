﻿namespace Inspection.Interface.Facade.Contracts.Question.Query
{
    public class QuestionListViewModel
    {
        public long Id { get; set; }
        public string Label { get; set; }
        public string Key { get; set; }
        public bool IsActive { get; set; }
        public string DepartmentName { get; set; }
        public string QuestionCategoryName { get; set; }
        public string ControllerType { get; set; }
    }
}