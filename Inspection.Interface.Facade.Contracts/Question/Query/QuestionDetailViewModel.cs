﻿namespace Inspection.Interface.Facade.Contracts.Question.Query
{
    public class QuestionDetailViewModel : QuestionListViewModel
    {
        public long QuestionCategoryId { get; set; }
        public long DepartmentId { get; set; }
    }
}