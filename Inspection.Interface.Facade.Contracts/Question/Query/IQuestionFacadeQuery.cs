﻿using System.Collections.Generic;

namespace Inspection.Interface.Facade.Contracts.Question.Query
{
    public interface IQuestionFacadeQuery
    {
        List<QuestionListViewModel> GetQuestions();
        QuestionDetailViewModel GetQuestionBy(long questionId);
    }
}
