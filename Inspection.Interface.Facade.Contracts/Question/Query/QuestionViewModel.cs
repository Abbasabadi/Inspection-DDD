﻿namespace Inspection.Interface.Facade.Contracts.Question.Query
{
    public class QuestionViewModel
    {
        public long Id { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public int Order { get; set; }
        public string ControllerType { get; set; }
    }
}