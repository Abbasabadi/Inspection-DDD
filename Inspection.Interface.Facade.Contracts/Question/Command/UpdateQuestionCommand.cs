﻿namespace Inspection.Interface.Facade.Contracts.Question.Command
{
    public class UpdateQuestionCommand
    {
        public long Id { get; set; }
        public string Label { get; set; }
        public string Key { get; set; }
        public bool IsRequired { get; set; }
        public string ControllerType { get; set; }
        public long DepartmentId { get; set; }
        public long QuestionCategoryId { get; set; }
    }
}