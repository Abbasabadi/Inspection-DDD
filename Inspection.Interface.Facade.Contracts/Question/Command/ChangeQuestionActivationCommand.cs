﻿namespace Inspection.Interface.Facade.Contracts.Question.Command
{
    public class ChangeQuestionActivationCommand
    {
        public long Id { get; set; }
        public bool IsActive { get; set; }
    }
}
