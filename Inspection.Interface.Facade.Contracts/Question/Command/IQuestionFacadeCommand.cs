﻿using Inspection.Interface.Facade.Contracts.Department.Command;

namespace Inspection.Interface.Facade.Contracts.Question.Command
{
    public interface IQuestionFacadeCommand
    {
        void CreateQuestion(CreateQuestionCommand command);
        void ChangeQuestionActivation(ChangeQuestionActivationCommand command);
        void UpdateQuestion(UpdateQuestionCommand command);
    }
}
