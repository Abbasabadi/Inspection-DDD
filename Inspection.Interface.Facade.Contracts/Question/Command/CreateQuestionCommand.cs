﻿using Framework.Application.Command;

namespace Inspection.Interface.Facade.Contracts.Question.Command
{
    public class CreateQuestionCommand: ICommand
    {
        public string Value { get; set; }
        public string Key { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public int Order { get; set; }
        public long DepartmentId { get; set; }
        public long QuestionCategoryId { get; set; }
        public string ControllerType { get; set; }
    }
}
