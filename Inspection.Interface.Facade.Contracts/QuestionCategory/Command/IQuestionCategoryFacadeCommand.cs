﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Interface.Facade.Contracts.QuestionCategory.Command
{
    public interface IQuestionCategoryFacadeCommand
    {
        void Create(CreateQuestionCategoryCommand command);
        void UpdateActivationStatus(ChangeQuestionCategoryActivationCommand command);
    }
}
