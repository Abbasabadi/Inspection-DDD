﻿using Framework.Application.Command;

namespace Inspection.Interface.Facade.Contracts.QuestionCategory.Command
{
    public class CreateQuestionCategoryCommand : ICommand
    {
        public string Name { get; set; }
        public long DepartmentId { get; set; }
    }
}