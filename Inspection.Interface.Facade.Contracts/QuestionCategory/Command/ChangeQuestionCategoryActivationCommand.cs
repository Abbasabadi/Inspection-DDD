﻿namespace Inspection.Interface.Facade.Contracts.QuestionCategory.Command
{
    public class ChangeQuestionCategoryActivationCommand
    {
        public long Id { get; set; }
        public bool IsActive { get; set; }

        public ChangeQuestionCategoryActivationCommand(long id, bool isActive)
        {
            Id = id;
            IsActive = isActive;
        }
    }
}
