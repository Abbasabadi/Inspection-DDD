﻿using System.Collections.Generic;

namespace Inspection.Interface.Facade.Contracts.QuestionCategory.Query
{
    public interface IQuestionCategoryFacadeQuery
    {
        List<QuestionCategoryViewModel> GetQuestionCategoriesWithQuestions(long departmentId);
        List<QuestionCategoryViewModel> GetQuestionCategoriesBy(long departmentId);
        List<QuestionCategoryViewModel> GetQuestionCategories();
    }
}
