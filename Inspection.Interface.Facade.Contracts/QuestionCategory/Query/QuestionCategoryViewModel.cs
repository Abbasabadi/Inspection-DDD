﻿using System.Collections.Generic;
using Inspection.Interface.Facade.Contracts.Question.Query;

namespace Inspection.Interface.Facade.Contracts.QuestionCategory.Query
{
    public class QuestionCategoryViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string DepartmentName { get; set; }
        public bool IsActive { get; set; }
        public List<QuestionViewModel> Questions { get; set; }
    }
}
