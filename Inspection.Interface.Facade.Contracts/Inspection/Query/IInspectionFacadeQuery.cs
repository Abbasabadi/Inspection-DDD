﻿namespace Inspection.Interface.Facade.Contracts.Inspection.Query
{
    public interface IInspectionFacadeQuery
    {
        InspectionViewModel GetInspection(long inspectionId);
        bool IsQuestionUsed(long questionId);
    }
}
