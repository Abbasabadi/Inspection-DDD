﻿using System;

namespace Inspection.Interface.Facade.Contracts.Inspection.Query
{
    public class InspectionViewModel
    {
        public long Id { get; set; }
        public string InspectorName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
