﻿using System;
using Framework.Application.Command;

namespace Inspection.Interface.Facade.Contracts.Inspection.Command
{
    public class CreateInspectionCommand : ICommand
    {
        public string InspectorName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long OrganizationId { get; set; }
        public long DepartmentId { get; set; }
    }
}