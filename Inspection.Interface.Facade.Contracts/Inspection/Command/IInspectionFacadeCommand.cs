﻿using System.Collections.Generic;

namespace Inspection.Interface.Facade.Contracts.Inspection.Command
{
    public interface IInspectionFacadeCommand
    {
        long CreateInspection(CreateInspectionCommand command);
        void PlaceQuestionAnswers(List<PlaceQuestionAnswerCommand> commands);
    }
}
