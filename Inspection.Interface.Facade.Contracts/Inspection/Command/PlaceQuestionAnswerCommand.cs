﻿using Framework.Application.Command;

namespace Inspection.Interface.Facade.Contracts.Inspection.Command
{
    public class PlaceQuestionAnswerCommand : ICommand
    {
        public string Answer { get; set; }
        public int QuestionId { get; set; }
        public int InspectionId { get; set; }
    }
}