﻿using Framework.Application.Command;

namespace Inspection.Interface.Facade.Contracts.Department.Command
{
    public class CreateDepartmentCommand: ICommand
    {
        public string Name { get; set; }
    }
}
