﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Interface.Facade.Contracts.Department.Command
{
    public interface IDepartmentFacadeCommand
    {
        void CreateDepartment(CreateDepartmentCommand command);
    }
}
