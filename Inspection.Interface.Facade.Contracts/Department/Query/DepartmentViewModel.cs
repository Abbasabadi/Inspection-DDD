﻿namespace Inspection.Interface.Facade.Contracts.Department.Query
{
    public class DepartmentViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}