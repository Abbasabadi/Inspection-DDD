﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Interface.Facade.Contracts.Department.Query
{
    public interface IDepartmentFacadeQuery
    {
        List<DepartmentViewModel> GetDepartments();
    }
}
