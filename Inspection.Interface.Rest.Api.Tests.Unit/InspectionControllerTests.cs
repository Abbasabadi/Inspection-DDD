﻿using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Inspection.Interface.Facade.Contracts.Inspection.Command;
using Inspection.Interface.Facade.Contracts.Inspection.Query;
using Moq;
using Xunit;

namespace Inspection.Interface.Rest.Api.Tests.Unit
{
    public class InspectionControllerTests
    {
        private long _inspectionId;
        private readonly Mock<IInspectionFacadeCommand> _facadeCommand;
        private readonly Mock<IInspectionFacadeQuery> _facadeQuery;
        private readonly Controllers.InspectionController _inspectionController;

        public InspectionControllerTests()
        {
            _facadeCommand = new Mock<IInspectionFacadeCommand>();
            _facadeQuery = new Mock<IInspectionFacadeQuery>();
            _inspectionController = new Controllers.InspectionController(_facadeCommand.Object, _facadeQuery.Object);
        }
        //Seperate this to 2 tests
        [Fact]
        public void Create_Inspection_Should_Call_Create_Inspection_On_Facade_Command_When_Create_Inspection_Command_Passed()
        {
            //Arrange
            var command = CreateInspectionCommandBuilder.Single();

            //Act
            _inspectionController.CreateInspection(command);

            //Assert
            _facadeCommand.Verify(x=>x.CreateInspection(command));
        }

        [Fact]
        public void Create_Inspection_Should_Return_Adding_Inspection_Id_When_Create_Inspection_Command_Passed()
        {
            //Arrange
            var command = CreateInspectionCommandBuilder.Single();

            //Act
            var result = _inspectionController.CreateInspection(command);

            //Assert
            Assert.IsType<long>(result);
        }

        [Fact]
        public void Place_Answers_Should_Call_Place_Question_Answers_On_Facade_When_Place_Question_Answer_Command_Collection_Are_Passed()
        {
            //Arrange
            var command = PlaceQuestionAnswersBuilder.Collection();

            //Act
            _inspectionController.PlaceAnswers(command);

            //Assert
            _facadeCommand.Verify(x=>x.PlaceQuestionAnswers(command));
        }


        [Fact]
        public void Get_Inspection_Should_Call_Get_Inspection_On_Facade_Query_When_Inspection_Id_Passed()
        {
            //Arrange
            _inspectionId = 4;

            //Act
            _inspectionController.GetInspection(_inspectionId);

            //Assert
            _facadeQuery.Verify(x=>x.GetInspection(_inspectionId));
        }

        [Fact]
        public void Should_Call_IsQuestionUsed_On_Facade_Query_When_Question_Id_Passed()
        {
            //Arrange
            var questionId = It.IsAny<long>();

            //Act
            _inspectionController.IsQuestionUsed(questionId);

            //Assert
            _facadeQuery.Verify(x=>x.IsQuestionUsed(questionId));
        }

        [Fact]
        public void Should_Return_Boolean_Value_When_Question_Id_Passed()
        {
            //Arrange
            const int questionId = 4;

            //Act
            var result =_inspectionController.IsQuestionUsed(questionId);

            //Assert
            Assert.IsType<bool>(result);
        }
    }
}