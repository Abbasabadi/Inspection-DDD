﻿using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Inspection.Interface.Facade.Contracts.Department.Command;
using Inspection.Interface.Facade.Contracts.Department.Query;
using Inspection.Interface.Rest.Api.Controllers;
using Moq;
using Xunit;

namespace Inspection.Interface.Rest.Api.Tests.Unit
{
    public class DepartmentControllerTests
    {
        private readonly Mock<IDepartmentFacadeCommand> _departmentFacadeCommand;
        private readonly Mock<IDepartmentFacadeQuery> _departmentFacadeQuery;
        private readonly DepartmentController _departmentController;

        public DepartmentControllerTests()
        {
            _departmentFacadeCommand = new Mock<IDepartmentFacadeCommand>();
            _departmentFacadeQuery = new Mock<IDepartmentFacadeQuery>();
            _departmentController = new DepartmentController(_departmentFacadeCommand.Object, _departmentFacadeQuery.Object);
        }
        [Fact]
        public void Create_Department_Should_Call_Create_Department_On_Facade_When_Create_Department_Command_Passed()
        {
            //Arrange
            var command = CreateDepartmentCommandBuilder.Single();

            //Act
            _departmentController.CreateDepartment(command);

            //Assert
            _departmentFacadeCommand.Verify(x=>x.CreateDepartment(command));
        }

        [Fact]
        public void Get_Departments_Should_Call_GetDepartments_On_Department_Query_Facade_And_Return_All_Departments()
        {
            //Arrange

            //Act
            _departmentController.GetDepartments();

            //Assert
            _departmentFacadeQuery.Verify(x=>x.GetDepartments());
        }
    }
}
