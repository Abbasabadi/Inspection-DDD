﻿using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;
using Inspection.Interface.Rest.Api.Controllers;
using Moq;
using Xunit;

namespace Inspection.Interface.Rest.Api.Tests.Unit
{
    public class QuestionCategoryControllerTests
    {
        private readonly Mock<IQuestionCategoryFacadeCommand> _facadeCommand;
        private readonly Mock<IQuestionCategoryFacadeQuery> _facadeQuery;
        private readonly QuestionCategoryController _controller;

        public QuestionCategoryControllerTests()
        {
            _facadeCommand = new Mock<IQuestionCategoryFacadeCommand>();
            _facadeQuery = new Mock<IQuestionCategoryFacadeQuery>();
            _controller = new QuestionCategoryController(_facadeCommand.Object, _facadeQuery.Object);
        }

        [Fact]
        public void CreateQuestionCategory_Should_Call_Create_On_Facade_Command_When_Create_Question_Category_Passed()
        {
            //Arrange
            var command = CreateQuestionCategoryCommandBuilder.Single();

            //Act
            _controller.CreateQuestionCategory(command);

            //Assert
            _facadeCommand.Verify(x => x.Create(command));
        }

        [Fact]
        public void GetQuestionCategories_Should_Call_GetQuestionCategoriesBy_On_Facade_Query_When_When_Department_Id_Passed()
        {
            //Arrange
            const long departmentId = 2;

            //Act
            _controller.GetQuestionCategories(departmentId);

            //Assert
            _facadeQuery.Verify(x => x.GetQuestionCategoriesBy(departmentId));
        }

        [Fact]
        public void GetQuestionCategoriesWithQuestions_Should_Call_Get_Question_Categories_With_Questions_On_Facade_Query_When_Department_Id_Passed()
        {
            //Arrange
            const long inspectionId = 4;

            //Act
            _controller.GetQuestionCategoriesWithQuestions(inspectionId);

            //Assert
            _facadeQuery.Verify(x => x.GetQuestionCategoriesWithQuestions(inspectionId));
        }

        [Fact]
        public void GetQuestionCategories_Should_Call_GetQuestionCategories_On_Facade_Query()
        {
            //Act
            _controller.GetQuestionCategories();

            //Assert
            _facadeQuery.Verify(x=>x.GetQuestionCategories());
        }

        [Fact]
        public void UpdateActivationStatus_Should_Call_Update_Activation_Status_On_Facade_Command_When_UpdateQuestionCategoryActivationCommand_Passed()
        {
            //Arrange
            var command = UpdateQuestionCategoryActivationCommandBuilder.Single();

            //Act
            _controller.ChangeActivationStatus(command);

            //Assert
            _facadeCommand.Verify(x=>x.UpdateActivationStatus(command));
        }
    }
}