﻿using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Inspection.Interface.Facade.Contracts.Question.Command;
using Inspection.Interface.Facade.Contracts.Question.Query;
using Moq;
using Xunit;

namespace Inspection.Interface.Rest.Api.Tests.Unit
{
    public class QuestionControllerTests
    {
        private readonly Mock<IQuestionFacadeCommand> _questionFacadeCommand;
        private readonly Mock<IQuestionFacadeQuery> _questionFacadeQuery;
        private readonly Controllers.QuestionController _controller;

        public QuestionControllerTests()
        {
            _questionFacadeCommand = new Mock<IQuestionFacadeCommand>();
            _questionFacadeQuery = new Mock<IQuestionFacadeQuery>();
            _controller =
                new Controllers.QuestionController(_questionFacadeCommand.Object, _questionFacadeQuery.Object);
        }

        [Fact]
        public void Create_Question_Should_Call_Create_Question_On_Facade_When_Create_Question_Passed()
        {
            //Arrange
            var command = CreateQuestionCommandBuilder.Single();

            //Act
            _controller.CreateQuestion(command);

            //Assert
            _questionFacadeCommand.Verify(x => x.CreateQuestion(command));
        }

        [Fact]
        public void Get_Questions_Should_Call_GetQuestions_On_Facade_And_Return_All_Questions()
        {
            //Arrange

            //Act
            _controller.GetQuestions();

            //Assert
            _questionFacadeQuery.Verify(x => x.GetQuestions());
        }

        [Fact]
        public void GetQuestion_Should_Call_GetQuestionBy_On_Facade_When_Question_Id_Passed()
        {
            //Arrange
            const long questionId = 1;

            //Act
            _controller.GetQuestion(questionId);

            //Assert
            _questionFacadeQuery.Verify(x => x.GetQuestionBy(questionId));
        }

        [Fact]
        public void
            ChangeQuestionActivation_Should_Call_ChangeQuestionActivation_On_Facade_Command_When_ChangeQuestionActivationCommand_Passed()
        {
            //Arrange
            var command = ChangeQuestionActivationCommandBuilder.Single();

            //Act
            _controller.ChangeQuestionActivation(command);

            //Assert
            _questionFacadeCommand.Verify(x => x.ChangeQuestionActivation(command));
        }

        [Fact]
        public void UpdateQuestion_Should_Call_UpdateQuestion_On_Facade_Command_When_UpdateQuestionCommand_Passed()
        {
            //Arrange
            var command = UpdateQuestionCommandBuilder.Single();

            //Act
            _controller.UpdateQuestion(command);

            //Assert
            _questionFacadeCommand.Verify(x => x.UpdateQuestion(command));
        }
    }
}