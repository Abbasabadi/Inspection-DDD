﻿using System;
using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Tests.Unit.Question;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Question
{
    public class CreateQuestionTests : RepositoryTestTearDown<Domain.Models.Question.Question, QuestionId>, IDisposable
    {
        private Domain.Models.Question.Question _question;
        private readonly QuestionRepository _questionRepository;

        public CreateQuestionTests() : base(SutDatabase.CSIS, SutCollection.Questions)
        {
            _questionRepository = new QuestionRepository(Database);
        }

        [Fact]
        public void Should_Add_New_Question_To_Database_When_Valid_Question_Passed()
        {
            //Arrange
            var id = Database.GetNextSequenceFrom("QuestionTestSeq");
            _question = new QuestionBuilder().WithId(id).Build();

            //Act
            _questionRepository.CreateQuestion(_question);
            var addedQuestion = _questionRepository.GetBy(id);

            //Assert
            Assert.Equal(_question, addedQuestion);
        }

        public void Dispose()
        {
            CleanUpInsertOperation(question => question.Id, _question.Id);
        }
    }
}