﻿using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Question
{
    public class GetByTests : RepositoryTestTearDown<Domain.Models.Question.Question, QuestionId>
    {
        private readonly QuestionRepository _questionRepository;

        public GetByTests() : base(SutDatabase.CSIS, SutCollection.Questions)
        {
            _questionRepository = new QuestionRepository(Database);
        }

        [Fact]
        public void Should_Return_Question_When_Question_Id_Passed()
        {
            //Arrange
            var questionId = GetQuestionId();

            //Act
            var result = _questionRepository.GetBy(questionId);

            //Assert
            Assert.IsType<Domain.Models.Question.Question>(result);
            Assert.Equal(questionId, result.Id.DbId);
        }

        private long GetQuestionId()
        {
            return MongoBag.GetEntityId<Domain.Models.Question.Question>(Database, SutCollection.Questions).Id.DbId;
        }
    }
}