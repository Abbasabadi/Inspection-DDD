﻿using System;
using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Tests.Unit.Question;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Question
{
    public class UpdateQuestionTests : FindAndReplaceTearDown<Domain.Models.Question.Question, QuestionId>, IDisposable
    {
        private readonly long _questionId;

        private readonly QuestionRepository _questionRepository;
        private readonly Domain.Models.Question.Question _initialedQuestion;
        private Domain.Models.Question.Question _expectedQuestion;

        public UpdateQuestionTests() : base(SutDatabase.CSIS, SutCollection.Questions)
        {
            _questionId = GetQuestionId();
            _questionRepository = new QuestionRepository(Database);
            _initialedQuestion = _questionRepository.GetBy(_questionId);
        }

        private long GetQuestionId()
        {
            return MongoBag.GetEntityId<Domain.Models.Question.Question>(Database, SutCollection.Questions).Id.DbId;
        }

        [Fact]
        public void Should_Update_Question_When_New_Question_Passed()
        {
            //Arrange
            const string expectedLabel = "something";
            _expectedQuestion = new QuestionBuilder().WithId(_questionId).WithLabel(expectedLabel).Build();

            //Act
            _questionRepository.UpdateQuesiton(_expectedQuestion);
            var result = _questionRepository.GetBy(_questionId);

            //Assert
            Assert.Equal(_initialedQuestion.Id.DbId, result.Id.DbId);
            Assert.Equal(expectedLabel, result.Label);
        }

        public void Dispose()
        {
            CleanUpFindAndReplaceOperation(x => x.Id.DbId, _questionId, _initialedQuestion);
        }
    }
}