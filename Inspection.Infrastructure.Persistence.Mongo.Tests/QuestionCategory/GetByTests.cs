﻿using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.QuestionCategory
{
    public class GetByTests: RepositoryTestTearDown<Domain.Models.QuestionCategory.QuestionCategory, QuestionCategoryId>
    {
        private readonly QuestionCategoryRepository _questionCategoryRepository;

        public GetByTests() : base(SutDatabase.CSIS, SutCollection.QuestionCategories)
        {
            _questionCategoryRepository = new QuestionCategoryRepository(Database);
        }

        [Fact]
        public void Should_Return_Question_Category_When_QuestionCategoryId_Passed()
        {
            //Arrange
            var questionCategoryId = GetQuestionCategoryId();

            //Act
            var questionCategory = _questionCategoryRepository.GetBy(questionCategoryId);

            //Assert
            Assert.IsType<Domain.Models.QuestionCategory.QuestionCategory>(questionCategory);
            Assert.NotNull(questionCategory.Name);
            Assert.Equal(questionCategoryId, questionCategory.Id.DbId);
        }

        private long GetQuestionCategoryId()
        {
            return MongoBag.GetEntityId<Domain.Models.QuestionCategory.QuestionCategory>(Database, SutCollection.QuestionCategories).Id.DbId;
        }
    }
}
