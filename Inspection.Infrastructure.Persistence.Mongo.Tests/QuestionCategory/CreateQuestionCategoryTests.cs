﻿using System;
using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Domain.Tests.Unit.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.QuestionCategory
{
    public class CreateQuestionCategoryTests : RepositoryTestTearDown<Domain.Models.QuestionCategory.QuestionCategory, QuestionCategoryId>,
        IDisposable
    {
        private Domain.Models.QuestionCategory.QuestionCategory _questionCategory;
        private readonly QuestionCategoryRepository _questionCategoryRepository;

        public CreateQuestionCategoryTests() : base(SutDatabase.CSIS, SutCollection.QuestionCategories)
        {
            _questionCategoryRepository = new QuestionCategoryRepository(Database);
        }

        [Fact]
        public void Should_Add_New_Question_Category_To_Database_When_Valid_Question_Category_Passed()
        {
            //Arrange
            var id = Database.GetNextSequenceFrom("QuestionCategoryTestSeq");
            _questionCategory = new QuestionCategoryBuilder().WithId(id).Build();

            //Act
            _questionCategoryRepository.Create(_questionCategory);
            var addedQuestionCategory = _questionCategoryRepository.GetBy(id);

            //Assert
            Assert.Equal(_questionCategory, addedQuestionCategory);
        }

        public void Dispose()
        {
            CleanUpInsertOperation(category => category.Id, _questionCategory.Id);
        }
    }
}