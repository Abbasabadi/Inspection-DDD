﻿using System;
using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Domain.Tests.Unit.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.QuestionCategory
{
    public class UpdateQuestionCategoryTests :
        FindAndReplaceTearDown<Domain.Models.QuestionCategory.QuestionCategory, QuestionCategoryId>,
        IDisposable
    {
        private readonly long _questionCategoryId;


        private readonly QuestionCategoryRepository _questionCategoryRepository;
        private readonly Domain.Models.QuestionCategory.QuestionCategory _initialQuestionCategory;
        private Domain.Models.QuestionCategory.QuestionCategory _expectedQuestionCategory;

        public UpdateQuestionCategoryTests() : base(SutDatabase.CSIS, SutCollection.QuestionCategories)
        {
            this._questionCategoryId = GetQuestionCategoryId();
            _questionCategoryRepository = new QuestionCategoryRepository(Database);
            _initialQuestionCategory = _questionCategoryRepository.GetBy(_questionCategoryId);
        }

        private long GetQuestionCategoryId()
        {
            return MongoBag
                .GetEntityId<Domain.Models.QuestionCategory.QuestionCategory>(Database,
                    SutCollection.QuestionCategories).Id.DbId;
        }

        [Fact]
        public void Should_Update_Question_Category_When_New_Question_Category_Passed()
        {
            //Arrange
            _expectedQuestionCategory =
                new QuestionCategoryBuilder().WithId(_questionCategoryId).WithName(Faker.NameFaker.Name()).Build();

            //Act
            _questionCategoryRepository.Update(_expectedQuestionCategory);
            var newQuestionCategory = _questionCategoryRepository.GetBy(_questionCategoryId);

            //Assert
            Assert.Equal(_expectedQuestionCategory.Id, newQuestionCategory.Id);
            Assert.Equal(_expectedQuestionCategory.Name, newQuestionCategory.Name);
            Assert.Equal(_expectedQuestionCategory.DepartmentId, newQuestionCategory.DepartmentId);
        }

        public void Dispose()
        {
            CleanUpFindAndReplaceOperation(x => x.Id.DbId, _questionCategoryId,
                _initialQuestionCategory);
        }
    }
}