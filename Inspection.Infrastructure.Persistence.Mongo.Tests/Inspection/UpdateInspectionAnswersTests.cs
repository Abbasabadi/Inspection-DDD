﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.MongoDb;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Tests.Unit.Inspection;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Inspection
{
    public class UpdateInspectionAnswersTests : RepositoryTestTearDown<Domain.Models.Inspection.Inspection, InspectionId>,
        IDisposable
    {
        private readonly long _inspectionId;
        private Domain.Models.Inspection.Inspection _expectedInspection;
        private readonly Domain.Models.Inspection.Inspection _initialedInspection;
        private readonly InspectionRepository _inspectionRepository;


        public UpdateInspectionAnswersTests() : base(SutDatabase.CSIS, SutCollection.Inspections)
        {
            this._inspectionId =
                MongoBag.GetEntityId<Domain.Models.Inspection.Inspection>(Database, SutCollection.Inspections).Id.DbId;
            _inspectionRepository = new InspectionRepository(Database);
            _initialedInspection = _inspectionRepository.GetById(_inspectionId);
        }

        [Fact]
        public void Should_Update_Inspection_Question_Answers_When_Inspection_Passed()
        {
            //Arrange

            var questionAnswers = TestQuestionAnswers();
            _expectedInspection = new InspectionBuilder().WithId(_inspectionId).WithQuestionAnswers(questionAnswers).Build();

            //Act
            _inspectionRepository.UpdateInspectionAnswers(_expectedInspection);
            var updatedInspection = _inspectionRepository.GetById(_expectedInspection.Id.DbId);

            //Assert
            Assert.Contains(_expectedInspection.QuestionAnswers.First(), updatedInspection.QuestionAnswers);
        }

        private static List<QuestionAnswer> TestQuestionAnswers()
        {
            return new List<QuestionAnswer>
            {
                new QuestionAnswer("True", 8),
                new QuestionAnswer("True", 9)
            };
        }

        public void Dispose()
        {
            CleanUpUpdateOperation(x => x.Id, _expectedInspection.Id, x => x.QuestionAnswers,
                _initialedInspection.QuestionAnswers);
        }
    }
}