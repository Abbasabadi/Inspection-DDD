﻿using System;
using Framework.MongoDb;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Tests.Unit.Inspection;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Inspection
{
    public class CreateInspectionTests : RepositoryTestTearDown<Domain.Models.Inspection.Inspection, InspectionId>, IDisposable
    {
        private Domain.Models.Inspection.Inspection _inspection;
        private readonly InspectionRepository _inspectionRepository;

        public CreateInspectionTests() : base(SutDatabase.CSIS, SutCollection.Inspections)
        {
            _inspectionRepository = new InspectionRepository(Database);
        }

        [Fact]
        public void Should_Add_New_Inspection_To_Database_When_Valid_Inspection_Passed()
        {
            //Arrange
            var id = Database.GetNextSequenceFrom("InspectionTestSeq");
            _inspection = new InspectionBuilder().WithId(id).Build();

            //Act
            _inspectionRepository.CreateInspection(_inspection);
            var addedInspection = _inspectionRepository.GetById(id);

            //Assert
            Assert.Equal(_inspection, addedInspection);
        }

        public void Dispose()
        {
            CleanUpInsertOperation(inspection => inspection.Id.DbId, _inspection.Id.DbId);
        }
    }
}