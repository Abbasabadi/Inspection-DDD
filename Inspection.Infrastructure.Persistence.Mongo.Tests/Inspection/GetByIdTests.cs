﻿using Framework.MongoDb;
using Inspection.Domain.Models.Inspection;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Inspection
{
    public class GetByIdTests : RepositoryTestTearDown<Domain.Models.Inspection.Inspection, InspectionId>
    {
        private readonly InspectionRepository _inspectionRepository;

        public GetByIdTests() : base(SutDatabase.CSIS, SutCollection.Inspections)
        {
            _inspectionRepository = new InspectionRepository(Database);
        }

        [Fact]
        public void Get_By_Id_Should_Return_Inspection_When_Inspection_Id_Is_Passed()
        {
            //Arrange
            var inspectionId = GetInspectionId();

            //Act
            var result = _inspectionRepository.GetById(inspectionId);

            //Assert
            Assert.Equal(inspectionId, result.Id.DbId);
        }

        private long GetInspectionId()
        {
            return MongoBag.GetEntityId<Domain.Models.Inspection.Inspection>(Database, SutCollection.Inspections).Id
                .DbId;
        }
    }
}