﻿using System;
using Framework.MongoDb;
using Inspection.Domain.Models.Department;
using Inspection.Domain.Tests.Unit.Department;
using Inspection.Infrastructure.Persistence.Mongo.Repositories;
using Xunit;

namespace Inspection.Infrastructure.Persistence.Mongo.Tests.Department
{
    public class CreateDepartmentTests : RepositoryTestTearDown<Domain.Models.Department.Department, DepartmentId>, IDisposable
    {
        private Domain.Models.Department.Department _department;
        private readonly DepartmentRepository _departmentRepository;

        public CreateDepartmentTests() : base(SutDatabase.CSIS, SutCollection.Departments)
        {
            _departmentRepository = new DepartmentRepository(Database);
        }

        [Fact]
        public void Should_Add_New_Department_To_Database_When_Valid_Department_Passed()
        {
            //Arrange
            var id = Database.GetNextSequenceFrom("DepartmentTestSeq");
            _department = new DepartmentBuilder().WithId(id).Build();

            //Act
            _departmentRepository.CreateDepartment(_department);
            
            //Assert
        }

        public void Dispose()
        {
            CleanUpInsertOperation(department => department.Id, _department.Id);
        }
    }
}