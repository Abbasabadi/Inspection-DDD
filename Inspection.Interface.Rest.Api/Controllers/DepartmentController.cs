﻿using System.Collections.Generic;
using System.Web.Http;
using Inspection.Interface.Facade.Contracts.Department.Command;
using Inspection.Interface.Facade.Contracts.Department.Query;

namespace Inspection.Interface.Rest.Api.Controllers
{
    public class DepartmentController : ApiController
    {
        private readonly IDepartmentFacadeCommand _departmentFacadeCommand;
        private readonly IDepartmentFacadeQuery _departmentFacadeQuery;

        public DepartmentController(IDepartmentFacadeCommand departmentFacadeCommand, IDepartmentFacadeQuery departmentFacadeQuery)
        {
            _departmentFacadeCommand = departmentFacadeCommand;
            _departmentFacadeQuery = departmentFacadeQuery;
        }

        [HttpPost]
        public void CreateDepartment(CreateDepartmentCommand command)
        {
            _departmentFacadeCommand.CreateDepartment(command);
        }

        [HttpGet]
        public List<DepartmentViewModel> GetDepartments()
        {
            return _departmentFacadeQuery.GetDepartments();
        }
    }
}