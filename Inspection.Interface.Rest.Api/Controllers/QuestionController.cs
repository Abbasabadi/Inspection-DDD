﻿using System.Collections.Generic;
using System.Web.Http;
using Inspection.Interface.Facade.Contracts.Question.Command;
using Inspection.Interface.Facade.Contracts.Question.Query;

namespace Inspection.Interface.Rest.Api.Controllers
{
    [RoutePrefix("api/Question")]
    public class QuestionController: ApiController
    {
        private readonly IQuestionFacadeCommand _questionFacadeCommand;
        private readonly IQuestionFacadeQuery _questionFacadeQuery;

        public QuestionController(IQuestionFacadeCommand questionFacadeCommand, IQuestionFacadeQuery questionFacadeQuery)
        {
            _questionFacadeCommand = questionFacadeCommand;
            _questionFacadeQuery = questionFacadeQuery;
        }

        [HttpPost]
        public void CreateQuestion(CreateQuestionCommand command)
        {
            _questionFacadeCommand.CreateQuestion(command);
        }

        [HttpGet]
        public List<QuestionListViewModel> GetQuestions()
        {
            return _questionFacadeQuery.GetQuestions();
        }

        [HttpGet]
        public QuestionDetailViewModel GetQuestion(long id)
        {
            return _questionFacadeQuery.GetQuestionBy(id);
        }

        [HttpPut]
        [Route("ChangeQuestionActivation")]
        public void ChangeQuestionActivation(ChangeQuestionActivationCommand command)
        {
            _questionFacadeCommand.ChangeQuestionActivation(command);
        }

        [HttpPut]
        [Route("UpdateQuestion")]
        public void UpdateQuestion(UpdateQuestionCommand command)
        {
            _questionFacadeCommand.UpdateQuestion(command);
        }
    }
}
