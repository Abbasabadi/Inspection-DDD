﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;

namespace Inspection.Interface.Rest.Api.Controllers
{
    [RoutePrefix("api/QuestionCategory")]
    public class QuestionCategoryController : ApiController
    {
        private readonly IQuestionCategoryFacadeCommand _questionCategoryFacadeCommand;
        private readonly IQuestionCategoryFacadeQuery _questionCategoryFacadeQuery;

        public QuestionCategoryController(IQuestionCategoryFacadeCommand questionCategoryFacadeCommand,
            IQuestionCategoryFacadeQuery questionCategoryFacadeQuery)
        {
            _questionCategoryFacadeCommand = questionCategoryFacadeCommand;
            _questionCategoryFacadeQuery = questionCategoryFacadeQuery;
        }

        [HttpPost]
        public void CreateQuestionCategory(CreateQuestionCategoryCommand command)
        {
            _questionCategoryFacadeCommand.Create(command);
        }

        [HttpPut]
        public void ChangeActivationStatus(ChangeQuestionCategoryActivationCommand command)
        {
            _questionCategoryFacadeCommand.UpdateActivationStatus(command);
        }

        [HttpGet]
        [Route("GetQuestionCategoriesWithQuestions/{id}")]
        public List<QuestionCategoryViewModel> GetQuestionCategoriesWithQuestions(long id)
        {
            return _questionCategoryFacadeQuery.GetQuestionCategoriesWithQuestions(id);
        }

        [HttpGet]
        [Route("GetQuestionCategories/{id}")]
        public List<QuestionCategoryViewModel> GetQuestionCategories(long id)
        {
            return _questionCategoryFacadeQuery.GetQuestionCategoriesBy(id);
        }

        [HttpGet]
        public List<QuestionCategoryViewModel> GetQuestionCategories()
        {
            return _questionCategoryFacadeQuery.GetQuestionCategories();
        }
    }
}