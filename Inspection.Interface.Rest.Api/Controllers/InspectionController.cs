﻿using System.Collections.Generic;
using System.Web.Http;
using Inspection.Interface.Facade.Contracts.Inspection.Command;
using Inspection.Interface.Facade.Contracts.Inspection.Query;

namespace Inspection.Interface.Rest.Api.Controllers
{
    public class InspectionController: ApiController
    {
        private readonly IInspectionFacadeCommand _inspectionFacadeCommand;
        private readonly IInspectionFacadeQuery _inspectionFacadeQuery;

        public InspectionController(IInspectionFacadeCommand inspectionFacadeCommand, IInspectionFacadeQuery inspectionFacadeQuery)
        {
            _inspectionFacadeCommand = inspectionFacadeCommand;
            _inspectionFacadeQuery = inspectionFacadeQuery;
        }

        [HttpPost]
        public long CreateInspection(CreateInspectionCommand command)
        {
            return _inspectionFacadeCommand.CreateInspection(command);
        }

        [HttpPut]
        public void PlaceAnswers(List<PlaceQuestionAnswerCommand> commands)
        {
            _inspectionFacadeCommand.PlaceQuestionAnswers(commands);
        }

        [HttpGet]
        public InspectionViewModel GetInspection(long id)
        {
            return _inspectionFacadeQuery.GetInspection(id);
        }

        [HttpGet]
        public bool IsQuestionUsed(long id)
        {
            return _inspectionFacadeQuery.IsQuestionUsed(id);
        }
    }
}
