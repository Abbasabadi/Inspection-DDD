﻿using Framework.Application.Command;
using Framework.Core.Events;
using Inspection.Domain.Contract;
using Inspection.Interface.Facade.Command;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Infrastructure.Facade.Command.Tests.Integration
{
    public class InspectionFacadeCommandIntegrationTests
    {
        [Fact]
        public void Create_Inspection_Should_Return_Inspection_Id_When_Create_Inspection_Command_Passed()
        {
            //Arrange
            var command = CreateInspectionCommandBuilder.Single();
            var commandBus = new Mock<ICommandBus>();
            var eventAggregator = new EventAggregator();
            var expectedEvent = new InspectionCreated(4);
            commandBus.Setup(x => x.Dispatch(command)).Callback(() => { eventAggregator.Publish(expectedEvent); });
            var inspectionFacade = new InspectionFacadeCommand(commandBus.Object, eventAggregator);

            //Act
            var result = inspectionFacade.CreateInspection(command);

            //Assert
            commandBus.VerifyAll();
            Assert.NotEqual(0, result);
            Assert.Equal(expectedEvent.Id, result);
        }
    }
}