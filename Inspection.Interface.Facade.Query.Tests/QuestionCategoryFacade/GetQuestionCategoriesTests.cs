﻿using System.Collections.Generic;
using System.Linq;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;
using Inspection.Interface.Facade.Query.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.QuestionCategoryFacade
{
    public class GetQuestionCategoriesTests
    {
        private const long DepartmentId = 2;
        private readonly Mock<IQuestionCategoryQuery> _questionCategoryQuery;
        private readonly Mock<IDepartmentQuery> _departmentQuery;
        private readonly QuestionCategoryFacadeQuery _questionCategoryFacadeQuery;

        public GetQuestionCategoriesTests()
        {
            _questionCategoryQuery = new Mock<IQuestionCategoryQuery>();
            _departmentQuery = new Mock<IDepartmentQuery>();
            var questionQuery = new Mock<IQuestionQuery>();
            _questionCategoryFacadeQuery =
                new QuestionCategoryFacadeQuery(_questionCategoryQuery.Object, questionQuery.Object, _departmentQuery.Object);
            _departmentQuery.Setup(x => x.GetDepartments()).Returns(FakeBag.FakeDepartments);
            _questionCategoryQuery.Setup(x => x.GetQuestionCategories())
                .Returns(FakeBag.FakeQuestionCategories);
        }

        [Fact]
        public void Should_Call_GetDepartments_And_GetQuestionCategories_On_Query()
        {
            //Arrange

            //Act
            _questionCategoryFacadeQuery.GetQuestionCategories();

            //Assert
            _questionCategoryQuery.Verify(x=>x.GetQuestionCategories());
            _departmentQuery.Verify(x=>x.GetDepartments());
        }

        [Fact]
        public void Should_Return_List_QuestionCategoryViewModel_With_DepartmentName()
        {
            //Arrange

            //Acr
            var result = _questionCategoryFacadeQuery.GetQuestionCategories();

            //Assert
            Assert.IsType<List<QuestionCategoryViewModel>>(result);
            Assert.Equal("حراست IT", result.First().DepartmentName);
        }
    }
}
