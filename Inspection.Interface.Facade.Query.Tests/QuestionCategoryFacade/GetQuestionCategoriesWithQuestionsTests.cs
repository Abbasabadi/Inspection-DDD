﻿using System.Collections.Generic;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;
using Inspection.Interface.Facade.Query.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.QuestionCategoryFacade
{
    public class GetQuestionCategoriesWithQuestionsTests
    {
        private const long DepartmentId = 4;
        private readonly Mock<IQuestionCategoryQuery> _questionCategoryQuery;
        private readonly Mock<IQuestionQuery> _questionQuery;
        private readonly QuestionCategoryFacadeQuery _questionCategoryFacadeQuery;

        public GetQuestionCategoriesWithQuestionsTests()
        {
            _questionCategoryQuery = new Mock<IQuestionCategoryQuery>();
            _questionQuery = new Mock<IQuestionQuery>();
            var departmentQuery = new Mock<IDepartmentQuery>();
            _questionCategoryFacadeQuery =
                new QuestionCategoryFacadeQuery(_questionCategoryQuery.Object, _questionQuery.Object,
                    departmentQuery.Object);
            _questionCategoryQuery.Setup(x => x.GetQuestionCategoriesBy(DepartmentId))
                .Returns(FakeBag.FakeQuestionCategories);
            _questionQuery.Setup(x => x.GetQuestionsByQuestionCategoryId(It.IsAny<long>()))
                .Returns(FakeBag.FakeQuestions);
        }

        [Fact]
        public void Should_Call_GetQuestionCategoriesBy_On_QuestionCategoryQuery()
        {
            //Arrange

            //Act
            _questionCategoryFacadeQuery.GetQuestionCategoriesWithQuestions(DepartmentId);

            //Assert
            _questionCategoryQuery.Verify(x => x.GetQuestionCategoriesBy(DepartmentId));
            _questionQuery.Verify(x => x.GetQuestionsByQuestionCategoryId(It.IsAny<long>()));
        }

        [Fact]
        public void Should_Return_Question_Categories_With_Its_Questions_When_Department_Id_Passed()
        {
            //Arrange

            //Act
            var result = _questionCategoryFacadeQuery.GetQuestionCategoriesWithQuestions(DepartmentId);

            //Assert
            Assert.IsType<List<QuestionCategoryViewModel>>(result);
        }
    }
}