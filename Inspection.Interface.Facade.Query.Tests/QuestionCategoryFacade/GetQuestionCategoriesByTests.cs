﻿using System;
using System.Collections.Generic;
using System.Linq;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Query;
using Inspection.Interface.Facade.Query.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.QuestionCategoryFacade
{
    public class GetQuestionCategoriesByTests
    {
        private const long DepartmentId = 2;
        private readonly Mock<IQuestionCategoryQuery> _questionCategoryQuery;
        private readonly QuestionCategoryFacadeQuery _questionCategoryFacadeQuery;

        public GetQuestionCategoriesByTests()
        {
            _questionCategoryQuery = new Mock<IQuestionCategoryQuery>();
            var questionQuery = new Mock<IQuestionQuery>();
            var departmentQuery = new Mock<IDepartmentQuery>();
            _questionCategoryFacadeQuery =
                new QuestionCategoryFacadeQuery(_questionCategoryQuery.Object, questionQuery.Object, departmentQuery.Object);
            _questionCategoryQuery.Setup(x => x.GetQuestionCategoriesBy(DepartmentId))
                .Returns(FakeBag.FakeQuestionCategories);
        }

        [Fact]
        public void
            Should_Call_GetQuestionCategoriesBy_On_QuestionCategoryQuery()
        {
            //Arrange

            //Act
            _questionCategoryFacadeQuery.GetQuestionCategoriesBy(DepartmentId);

            //Assert
            _questionCategoryQuery.Verify(x => x.GetQuestionCategoriesBy(DepartmentId));
        }

        [Fact]
        public void Should_Return_List_Question_Category_View_Model_With_Out_Questions_When_Department_Id_Passed()
        {
            //Arrange


            //Act
            var result = _questionCategoryFacadeQuery.GetQuestionCategoriesBy(DepartmentId);

            //Assert
            Assert.IsType<List<QuestionCategoryViewModel>>(result);
            Assert.Empty(result.First().Questions);
        }
    }
}