﻿using System.Collections.Generic;
using Inspection.Domain.Models.Department;
using Inspection.Domain.Tests.Unit.Department;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Interface.Facade.Contracts.Department.Query;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.DepartmentFacade
{
    public class GetDepartmentsTests
    {
        [Fact]
        public void Should_Call_GetDepartments_On_DepartmentFacadeQuery_And_Return_All_Departments()
        {
            //Arrange
            var departmentQuery = new Mock<IDepartmentQuery>();
            var facade = new DepartmentFacadeQuery(departmentQuery.Object);
            departmentQuery.Setup(x => x.GetDepartments()).Returns(FakeDepartments);
            //Act
            var result = facade.GetDepartments();

            //Assert
            departmentQuery.Verify(x=>x.GetDepartments());
            Assert.IsType<List<DepartmentViewModel>>(result);
        }

        public static List<Department> FakeDepartments()
        {
            return new List<Department>
            {
                new DepartmentBuilder().WithId(10).WithName("حراست IT").Build(),
                new DepartmentBuilder().WithId(11).WithName("حفاظت پرسنلی").Build()
            };
        }
    }
}
