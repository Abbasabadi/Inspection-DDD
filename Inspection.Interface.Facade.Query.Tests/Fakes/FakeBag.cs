﻿using System.Collections.Generic;
using Inspection.Domain.Models.Department;
using Inspection.Domain.Models.Inspection;
using Inspection.Domain.Models.Question;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Domain.Tests.Unit.Department;
using Inspection.Domain.Tests.Unit.Inspection;
using Inspection.Domain.Tests.Unit.Question;
using Inspection.Domain.Tests.Unit.QuestionCategory;

namespace Inspection.Interface.Facade.Query.Tests.Unit.Fakes
{
    public static class FakeBag
    {
        public static List<Domain.Models.Inspection.Inspection> FakeInspections()
        {
            var questionAnswers = new List<QuestionAnswer>
            {
                new QuestionAnswer("changedTest1", 6),
                new QuestionAnswer("changedTest2", 7),
                new QuestionAnswer("changedTest3", 8)
            };
            return new List<Domain.Models.Inspection.Inspection>
            { 
                new InspectionBuilder().WithId(1).WithQuestionAnswers(questionAnswers).Build()
            };
        }

        public static List<Department> FakeDepartments()
        {
            return new List<Department>
            {
                new DepartmentBuilder().WithId(10).WithName("حراست IT").Build(),
                new DepartmentBuilder().WithId(11).WithName("حفاظت پرسنلی").Build()
            };
        }

        public static Department FakeDepartment()
        {
            return new DepartmentBuilder().WithId(10).WithName("حراست IT").Build();
        }

        public static List<QuestionCategory> FakeQuestionCategories()
        {
            return new List<QuestionCategory>
            {
                new QuestionCategoryBuilder().WithId(100).WithName("اتاق سرور").WithDepartmentId(10).Build(),
                new QuestionCategoryBuilder().WithId(101).WithName("وضعیت پرونده ها").WithDepartmentId(11).Build()
            };
        }

        public static QuestionCategory FakeQuestionCategory()
        {
            return new QuestionCategoryBuilder().WithId(100).WithName("اتاق سرور").WithDepartmentId(10).Build();
        }

        public static List<Question> FakeQuestions()
        {
            return new List<Question>
            {
                new QuestionBuilder().WithId(1).WithQuestionCategoryId(100).WithDepartmentId(10).Build(),
                new QuestionBuilder().WithId(2).WithQuestionCategoryId(101).WithDepartmentId(11).Build()
            };
        }

        public static Question FakeQuestion()
        {
            return new QuestionBuilder().WithId(1).WithQuestionCategoryId(100).WithDepartmentId(10).Build();
        }
    }
}