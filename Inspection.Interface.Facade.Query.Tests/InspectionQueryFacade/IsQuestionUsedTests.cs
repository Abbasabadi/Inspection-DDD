﻿using Inspection.Infrastructure.Query.Mongo.Inspection;
using Inspection.Interface.Facade.Query.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.InspectionQueryFacade
{
    public class IsQuestionUsedTests
    {
        private long _questionId;
        private readonly Mock<IInspectionQuery> _inspectionQuery;
        private readonly InspectionFacadeQuery _inspectionFacadeQuery;

        public IsQuestionUsedTests()
        {
            _inspectionQuery = new Mock<IInspectionQuery>();
            _inspectionFacadeQuery = new InspectionFacadeQuery(_inspectionQuery.Object);
            var inspections = FakeBag.FakeInspections();
            _inspectionQuery.Setup(x => x.GetInspections()).Returns(inspections);

        }
        [Fact]
        public void Should_Return_False_When_Question_Is_Not_Used_In_Any_Inspection()
        {
            //Arrange
            _questionId = 2;
            

            //Act
            var result = _inspectionFacadeQuery.IsQuestionUsed(_questionId);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void Should_Return_True_When_Question_Is_Used_In_Inspections()
        {
            //Arrange
            _questionId = 7;

            //Act
            var result = _inspectionFacadeQuery.IsQuestionUsed(_questionId);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void Should_Call_Get_Inspections_On_Facade_Query()
        {
            //Arrange
            _questionId = It.IsAny<long>();

            //Act
            _inspectionFacadeQuery.IsQuestionUsed(_questionId);

            //Assert
            _inspectionQuery.Verify(x=>x.GetInspections());
        }
    }
}
