﻿using Inspection.Domain.Tests.Unit.Inspection;
using Inspection.Infrastructure.Query.Mongo.Inspection;
using Inspection.Interface.Facade.Contracts.Inspection.Query;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.InspectionQueryFacade
{
    public class GetInspectionTests
    {
        [Fact]
        public void
            Should_Call_GetInspection_On_InspectionQuery_And_Return_Inspection_View_Model_When_Inspection_id_Passed()
        {
            //Arrange
            const long inspectionId = 4;
            var inspectionQuery = new Mock<IInspectionQuery>();
            var inspectionFacade = new InspectionFacadeQuery(inspectionQuery.Object);
            inspectionQuery.Setup(x => x.GetInspection(inspectionId)).Returns(GetInspectionReturnData);

            //Act
            var result = inspectionFacade.GetInspection(inspectionId);

            //Assert
            inspectionQuery.Verify(x=>x.GetInspection(inspectionId));
            Assert.IsType<InspectionViewModel>(result);
        }

        public static Domain.Models.Inspection.Inspection GetInspectionReturnData()
        {
            return new InspectionBuilder().Build();
        }
    }
}