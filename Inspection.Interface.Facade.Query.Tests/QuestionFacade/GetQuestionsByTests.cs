﻿using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.Question.Query;
using Inspection.Interface.Facade.Query.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.QuestionFacade
{
    public class GetQuestionsByTests
    {
        private readonly Mock<IQuestionQuery> _questionQueryMock;
        private readonly Mock<IQuestionCategoryQuery> _questionCategoryMock;
        private readonly Mock<IDepartmentQuery> _departmetnQueryMock;
        private readonly QuestionFacadeQuery _questionFacadeQuery;
        private const long ExpectedQuestionId = 1;
        private const long ExpectedQuestionCategoryId = 100;
        private const long ExpectedDepartmentId = 10;

        public GetQuestionsByTests()
        {
            _questionQueryMock = new Mock<IQuestionQuery>();
            _questionCategoryMock = new Mock<IQuestionCategoryQuery>();
            _departmetnQueryMock = new Mock<IDepartmentQuery>();
            _questionFacadeQuery = new QuestionFacadeQuery(_questionQueryMock.Object, _questionCategoryMock.Object,
                _departmetnQueryMock.Object);
            SetUp();
        }

        private void SetUp()
        {
            _questionQueryMock.Setup(x => x.GetQuestionBy(ExpectedQuestionId)).Returns(FakeBag.FakeQuestion);
            _questionCategoryMock.Setup(x => x.GetQuestionCategoryBy(ExpectedQuestionCategoryId))
                .Returns(FakeBag.FakeQuestionCategory);
            _departmetnQueryMock.Setup(x => x.GetDepartmentBy(ExpectedDepartmentId)).Returns(FakeBag.FakeDepartment);
        }

        [Fact]
        public void Should_Return_QuestionListViewModel_When_Question_Id_Passed()
        {
            //Arrange

            //Act
            var result = _questionFacadeQuery.GetQuestionBy(ExpectedQuestionId);

            //Assert
            Assert.IsType<QuestionDetailViewModel>(result);
            Assert.Equal(ExpectedQuestionId, result.Id);
            Assert.NotNull(result.DepartmentName);
            Assert.Equal(ExpectedDepartmentId, result.DepartmentId);
            Assert.NotNull(result.QuestionCategoryName);
            Assert.Equal(ExpectedQuestionCategoryId, result.QuestionCategoryId);
        }

        [Fact]
        public void Should_Call_GetQuestionBy_And_GetQuestionCategoryBy_And_GetDepartmentBy_On_Query()
        {
            //Arrange

            //Act
            _questionFacadeQuery.GetQuestionBy(ExpectedQuestionId);

            //Assert
            _questionQueryMock.Verify(x => x.GetQuestionBy(ExpectedQuestionId));
            _questionCategoryMock.Verify(x => x.GetQuestionCategoryBy(ExpectedQuestionCategoryId));
            _departmetnQueryMock.Verify(x => x.GetDepartmentBy(ExpectedDepartmentId));
        }
    }
}