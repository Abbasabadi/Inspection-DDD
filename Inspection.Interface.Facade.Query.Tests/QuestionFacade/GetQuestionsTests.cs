﻿using System.Collections.Generic;
using System.Linq;
using Inspection.Infrastructure.Query.Mongo.Department;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Inspection.Interface.Facade.Contracts.Question.Query;
using Inspection.Interface.Facade.Query.Tests.Unit.Fakes;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Query.Tests.Unit.QuestionFacade
{
    public class GetQuestionsTests
    {
        private readonly Mock<IQuestionQuery> _questionQueryMock;
        private readonly Mock<IQuestionCategoryQuery> _questionCategoryMock;
        private readonly Mock<IDepartmentQuery> _departmetnQueryMock;
        public GetQuestionsTests()
        {
            _questionQueryMock = new Mock<IQuestionQuery>();
            _questionCategoryMock = new Mock<IQuestionCategoryQuery>();
            _departmetnQueryMock = new Mock<IDepartmentQuery>();
        }

        [Fact]
        public void Should_Return_List_QuestionListViewModel()
        {
            //Arrange
            var questionFacade = CreateQuestionFacadeObject();
            var expectedDepartment = FakeBag.FakeDepartments().First().Name;
            var expectedQuestionCategory = FakeBag.FakeQuestionCategories().First(x => x.Id.DbId == 101).Name;
            
            //Act
            var result = questionFacade.GetQuestions();

            //Assert
            Assert.IsType<List<QuestionListViewModel>>(result);
            Assert.Equal(expectedDepartment, result.First().DepartmentName);
            Assert.Equal(expectedQuestionCategory, result.First(x=>x.Id == 2).QuestionCategoryName);
        }

        [Fact]
        public void Should_Call_GetDepartments_And_GetQuestionCategories_And_GetQuestions_On_Query()
        {
            //Arrange
            var questionFacadeQuery = CreateQuestionFacadeObject();
            

            //Act
            questionFacadeQuery.GetQuestions();

            //Assert
            _questionQueryMock.Verify(x=>x.GetQuestions());
            _questionCategoryMock.Verify(x=>x.GetQuestionCategories());
            _departmetnQueryMock.Verify(x=>x.GetDepartments());
        }

        private QuestionFacadeQuery CreateQuestionFacadeObject()
        {
            var questionFacadeQuery =  new QuestionFacadeQuery(_questionQueryMock.Object, _questionCategoryMock.Object, _departmetnQueryMock.Object);

            _questionQueryMock.Setup(x => x.GetQuestions()).Returns(FakeBag.FakeQuestions);
            _questionCategoryMock.Setup(x => x.GetQuestionCategories()).Returns(FakeBag.FakeQuestionCategories);
            _departmetnQueryMock.Setup(x => x.GetDepartments()).Returns(FakeBag.FakeDepartments);

            return questionFacadeQuery;
        }
    }
}