﻿using Framework.MongoDb;
using Inspection.Domain.Models.Department;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Department;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Department
{
    public class GetDepartmentByTests: QueryTestsBase<Domain.Models.Department.Department, DepartmentId>
    {
        private readonly DepartmentQuery _departmentQuery;

        public GetDepartmentByTests(): base(SutDatabase.CSIS)
        {
            _departmentQuery = new DepartmentQuery(Database);
        }

        [Fact]
        public void Should_Return_Department_When_Department_Id_Is_Passed()
        {
            //Arrange
            var departmentId = GetDepartmentId();

            //Act
            var result = _departmentQuery.GetDepartmentBy(departmentId);

            //Assert
            Assert.IsType<Domain.Models.Department.Department>(result);
            Assert.Equal(departmentId, result.Id.DbId);
        }

        private long GetDepartmentId()
        {
            return MongoBag.GetEntityId<Domain.Models.Department.Department>(Database, SutCollection.Departments).Id.DbId;
        }
    }
}
