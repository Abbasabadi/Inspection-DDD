﻿using System.Collections.Generic;
using Framework.MongoDb;
using Inspection.Domain.Models.Department;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Department;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Department
{
    public class GetDepartmentsTests : QueryTestsBase<Domain.Models.Department.Department, DepartmentId>
    {
        public GetDepartmentsTests() : base(SutDatabase.CSIS)
        {
        }

        [Fact]
        public void Should_Return_All_Departments()
        {
            //Arrange
            var departmentQuery = new DepartmentQuery(Database);

            //Act
            var result = departmentQuery.GetDepartments();

            //Assert
            Assert.IsType<List<Domain.Models.Department.Department>>(result);
        }
    }
}