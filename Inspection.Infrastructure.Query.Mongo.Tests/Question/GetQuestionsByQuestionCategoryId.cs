﻿using System.Collections.Generic;
using System.Linq;
using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Question
{
    public class GetQuestionsByQuestionCategoryId : QueryTestsBase<Domain.Models.Question.Question, QuestionId>
    {
        private readonly QuestionQuery _questionQuery;

        public GetQuestionsByQuestionCategoryId() : base(SutDatabase.CSIS)
        {
            _questionQuery = new QuestionQuery(Database);
        }

        [Fact]
        public void Should_Return_Active_Questions_When_Question_Category_Id_Passed()
        {
            //Arrange
            var questionCategoryId = GetQuestionCategoryId();

            //Act
            var result = _questionQuery.GetQuestionsByQuestionCategoryId(questionCategoryId);

            //Assert
            Assert.InRange(result.Count, 0, long.MaxValue);
            Assert.IsType<List<Domain.Models.Question.Question>>(result);
            Assert.Equal(questionCategoryId, result.First().QuestionCategoryId.Value);
        }

        private long GetQuestionCategoryId()
        {
            return MongoBag
                .GetEntityId<Domain.Models.QuestionCategory.QuestionCategory>(Database, SutCollection.QuestionCategories).Id
                .DbId;
        }
    }
}