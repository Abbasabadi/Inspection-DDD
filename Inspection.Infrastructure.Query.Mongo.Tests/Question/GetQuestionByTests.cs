﻿using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Question
{
    public class GetQuestionByTests : QueryTestsBase<Domain.Models.Question.Question, QuestionId>
    {
        private readonly QuestionQuery _questionQuery;

        public GetQuestionByTests() : base(SutDatabase.CSIS)
        {
            _questionQuery = new QuestionQuery(Database);
        }

        [Fact]
        public void Should_Return_Single_Question_When_Question_Id_Passed()
        {
            //Arrange
            var questionId = GetQuestionId();

            //Act
            var result = _questionQuery.GetQuestionBy(questionId);

            //Assert
            Assert.Equal(questionId, result.Id.DbId);
            Assert.IsType<Domain.Models.Question.Question>(result);
        }

        private long GetQuestionId()
        {
            return MongoBag.GetEntityId<Domain.Models.Question.Question>(Database, SutCollection.Questions).Id.DbId;
        }
    }
}