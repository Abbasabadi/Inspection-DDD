﻿using System.Collections.Generic;
using Framework.MongoDb;
using Inspection.Domain.Models.Question;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Qustion;
using MongoDB.Driver;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Question
{
    public class GetQuestionsTests : QueryTestsBase<Domain.Models.Question.Question, QuestionId>
    {
        private readonly QuestionQuery _questionQuery;

        public GetQuestionsTests() : base(SutDatabase.CSIS)
        {
            _questionQuery = new QuestionQuery(Database);
        }

        [Fact]
        public void Should_Return_All_Questions()
        {
            //Arrange

            //Act
            var result = _questionQuery.GetQuestions();

            //Assert
            Assert.IsType<List<Domain.Models.Question.Question>>(result);
        }
    }
}