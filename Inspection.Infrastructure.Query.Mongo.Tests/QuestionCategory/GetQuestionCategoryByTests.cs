﻿using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.QuestionCategory
{
    public class GetQuestionCategoryByTests : QueryTestsBase<Domain.Models.QuestionCategory.QuestionCategory, QuestionCategoryId>
    {
        private readonly QuestionCategoryQuery _questionCategoryQuery;

        public GetQuestionCategoryByTests() : base(SutDatabase.CSIS)
        {
            _questionCategoryQuery = new QuestionCategoryQuery(Database);
        }

        [Fact]
        public void Should_Return_Question_Category_When_Question_Category_Id_Passed()
        {
            //Arrange
            var questionCategoryId = GetQuestionCategoryId();

            //Act
            var result = _questionCategoryQuery.GetQuestionCategoryBy(questionCategoryId);

            //Assert
            Assert.IsType<Domain.Models.QuestionCategory.QuestionCategory>(result);
            Assert.Equal(questionCategoryId, result.Id.DbId);
        }

        private long GetQuestionCategoryId()
        {
            return MongoBag
                .GetEntityId<Domain.Models.QuestionCategory.QuestionCategory>(Database,
                    SutCollection.QuestionCategories).Id.DbId;
        }
    }
}