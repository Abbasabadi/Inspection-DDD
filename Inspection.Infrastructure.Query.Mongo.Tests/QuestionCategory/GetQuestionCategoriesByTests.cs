﻿using System.Collections.Generic;
using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.QuestionCategory
{
    public class GetQuestionCategoriesByTests : QueryTestsBase<Domain.Models.QuestionCategory.QuestionCategory, QuestionCategoryId>
    {
        private readonly QuestionCategoryQuery _questionCategoryQuery;

        public GetQuestionCategoriesByTests() : base(SutDatabase.CSIS)
        {
            _questionCategoryQuery = new QuestionCategoryQuery(Database);
        }

        [Fact]
        public void Should_Return_Question_Categories_When_Department_Id_Passed()
        {
            //Arrange
            var departmentId = GetDepartmentId();

            //Act
            var result = _questionCategoryQuery.GetQuestionCategoriesBy(departmentId);

            //Assert
            Assert.InRange(result.Count, 0, long.MaxValue);
            Assert.IsType<List<Domain.Models.QuestionCategory.QuestionCategory>>(result);
        }

        private long GetDepartmentId()
        {
            return MongoBag
                .GetEntityId<Domain.Models.Department.Department>(Database, SutCollection.Departments).Id
                .DbId;
        }
    }
}