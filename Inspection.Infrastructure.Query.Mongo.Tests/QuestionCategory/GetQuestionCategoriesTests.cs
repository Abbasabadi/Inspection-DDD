﻿using System.Collections.Generic;
using Framework.MongoDb;
using Inspection.Domain.Models.QuestionCategory;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.QuestionCategory;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.QuestionCategory
{
    public class GetQuestionCategoriesTests : QueryTestsBase<Domain.Models.QuestionCategory.QuestionCategory, QuestionCategoryId>
    {
        private readonly QuestionCategoryQuery _questionCategoryQuery;

        public GetQuestionCategoriesTests() : base(SutDatabase.CSIS)
        {
            _questionCategoryQuery = new QuestionCategoryQuery(Database);
        }

        [Fact]
        public void Should_Return_All_Question_Categories()
        {
            //Arrange

            //Act
            var result = _questionCategoryQuery.GetQuestionCategories();

            //Assert
            Assert.IsType<List<Domain.Models.QuestionCategory.QuestionCategory>>(result);
        }
    }
}