﻿using Framework.MongoDb;
using Inspection.Domain.Models.Inspection;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Inspection;
using MongoDB.Driver;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Inspection
{
    public class GetInspectionTests : QueryTestsBase<Domain.Models.Inspection.Inspection, InspectionId>
    {
        private readonly InspectionQuery _inspectionQuery;

        public GetInspectionTests():base(SutDatabase.CSIS)
        {
            _inspectionQuery = new InspectionQuery(Database);
        }
        
        [Fact]
        public void Should_Return_Inspection_When_Inspection_Id_Passed()
        {
            //Arrange
            var inspectionId = GetInspectionId();

            //Act
            var result = _inspectionQuery.GetInspection(inspectionId);

            //Assert
            Assert.Equal(result.Id.DbId, inspectionId);
            Assert.IsType<Domain.Models.Inspection.Inspection>(result);
        }

        private long GetInspectionId()
        {
            return MongoBag.GetEntityId<Domain.Models.Inspection.Inspection>(Database,SutCollection.Inspections).Id.DbId;
        }
    }
}
