﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inspection.Infrastructure.Persistence.Mongo;
using Inspection.Infrastructure.Query.Mongo.Inspection;
using MongoDB.Driver;
using Xunit;

namespace Inspection.Infrastructure.Query.Mongo.Tests.Inspection
{
    public class GetInspectionsTests
    {
        private readonly IMongoDatabase _database;

        public GetInspectionsTests()
        {
            _database = new MongoClient().GetDatabase(SutDatabase.CSIS);
        }

        [Fact]
        public void Should_Return_All_Inspections()
        {
            //Arrange
            var inspectionQuery = new InspectionQuery(_database);

            //Act
            var result = inspectionQuery.GetInspections();

            //Assert
            Assert.IsType<List<Domain.Models.Inspection.Inspection>>(result);
        }
    }
}
