﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasicInformation.@interface.RestApi;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace BasicInformation.Infrastructure.Config
{
    public static class BasicInformationBootstrapper
    {
        public static void WireUp(IWindsorContainer container)
        {
            container.Register(Component.For<BasicInformationController>().LifestylePerWebRequest());
        }
    }
}
