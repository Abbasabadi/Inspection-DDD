﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;
using Organization.Interface.Facade.Contract.Organizations.Query;

namespace Organization.Interface.Facade.Query.Tests.Unit.Builders
{
    public static class OrganizationViewModelBuilder
    {
        public static OrganizationViewModel Single()
        {
            return Builder<OrganizationViewModel>.CreateNew()
                .With(x=>x.Id, 2)
                .With(x=>x.Name, "وزارت راه و شهرسازی")
                .With(x=>x.IsHeadQuarter, true)
                .Build();
        }

        public static List<OrganizationViewModel> Collection()
        {
            return Builder<OrganizationViewModel>.CreateListOfSize(3)
                .All()
                .With(x=>x.Id, 1)
                .With(x=>x.Name,"وزارت خانه ها")
                .With(x=>x.IsHeadQuarter, true)
                .Build()
                .ToList();
        }
    }
}