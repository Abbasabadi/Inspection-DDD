﻿using System.Collections.Generic;
using MongoDB.Driver;
using Moq;
using Organization.Domain.Tests.Unit.Organization;
using Organization.Interface.Facade.Contract.Organizations.Query;
using Organization.Interface.Facade.Query.Tests.Unit.Builders;
using Orgnaization.Infrastructure.Query.Mongo;
using Xunit;

namespace Organization.Interface.Facade.Query.Tests.Unit
{
    public class OrganizationFacadeQueryTests
    {
        [Fact]
        public void
            Get_Organization_Should_Call_GetOrganizationById_On_Organization_Query_And_Return_Organization_View_Model_When_Id_Passed()
        {
            //Arrange
            const long id = 1;
            var organizationQuery = new Mock<IOrganizationQuery>();
            var facade = new OrganizationFacadeQuery(organizationQuery.Object);
            var organizationBuilder = new OrganizationBuilder();
            organizationQuery.Setup(x => x.GetOrganization(id)).Returns(organizationBuilder.Build);

            //Act
            var result = facade.GetOrganization(id);

            //Assert
            organizationQuery.Verify(x => x.GetOrganization(id));
            Assert.IsType<OrganizationViewModel>(result);
        }

        [Fact]
        public void
            Get_Organizations_Should_Call_GetOrganizations_On_Organization_Query_And_Return_List_Organization_View_Model_When_Id_Passed()
        {
            //Arrange
            var organizationQuery = new Mock<IOrganizationQuery>();
            var facade = new OrganizationFacadeQuery(organizationQuery.Object);
            var organizationBuilder = new OrganizationBuilder();
            organizationQuery.Setup(x => x.GetOrganizations())
                .Returns(organizationBuilder.BuildCollection);
            
            //Act
            var result = facade.GetOrganizations();

            //Assert
            organizationQuery.Verify(x => x.GetOrganizations());
            Assert.IsType<List<OrganizationViewModel>>(result);
        }
    }
}