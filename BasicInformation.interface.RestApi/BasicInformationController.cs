﻿using System.Collections.Generic;
using System.Web.Http;
using BasicInformation.Application;

namespace BasicInformation.@interface.RestApi
{
    public class BasicInformationController: ApiController
    {
        private readonly BasicInformationApplication _basicInformationApplication;

        public BasicInformationController()
        {
            _basicInformationApplication = new BasicInformationApplication();
        }
        [HttpGet]
        public List<ControllerType> GetControllerTypes()
        {
            return _basicInformationApplication.GetControllerTypes();
        }
    }
}
