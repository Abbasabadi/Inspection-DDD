﻿using Moq;
using Organization.Interface.Facad.Command.Tests.Unit.CommandBuilders;
using Organization.Interface.Facade.Contract.Organizations.Command;
using Organization.Interface.Facade.Contract.Organizations.Query;
using Organization.Interface.Facade.Query.Tests.Unit.Builders;
using Xunit;

namespace Organization.Interface.Rest.Api.Tests.Unit
{
    public class OrganizationControllerTests
    {
        [Fact]
        public void
            Create_Organization_Should_Call_Create_Organization_On_Facade_Command_When_Create_Organization_Command_Passed()
        {
            //Arrange
            var facadeCommand = new Mock<IOrganizationFacadeCommand>();
            var facadeQuery = new Mock<IOrganizationFacadeQuery>();
            var controller = new OrganizationController(facadeCommand.Object, facadeQuery.Object);
            var command = CreateOrganizationCommandBuilder.Single();

            //Act
            controller.CreateOrganization(command);

            //Assert
            facadeCommand.Verify(x => x.CreateOrganization(command));
        }

        [Fact]
        public void
            Get_Organization_Should_Call_Get_Organization_And_Returns_Organization_Query_On_Facade_Command_When_Id_Passed()
        {
            //Arrange
            const long id = 1;
            var facadeCommand = new Mock<IOrganizationFacadeCommand>();
            var facadeQuery = new Mock<IOrganizationFacadeQuery>();
            var controller = new OrganizationController(facadeCommand.Object, facadeQuery.Object);
            var expected = OrganizationViewModelBuilder.Single();
            facadeQuery.Setup(x => x.GetOrganization(id)).Returns(expected);

            //Act
            var result = controller.GetOrganization(id);

            //Assert
            facadeQuery.Verify(x => x.GetOrganization(id));
            Assert.Same(expected, result);
            Assert.Equal(expected.Id, result.Id);
        }

        [Fact]
        public void
            Get_Organizations_Should_Call_Get_Organizations_And_Returns_List_Organization_Query_On_Facade_Command()
        {
            //Arrange
            var facadeCommand = new Mock<IOrganizationFacadeCommand>();
            var facadeQuery = new Mock<IOrganizationFacadeQuery>();
            var controller = new OrganizationController(facadeCommand.Object, facadeQuery.Object);

            //Act
            controller.GetOrganizations();

            //Assert
            facadeQuery.Verify(x => x.GetOrganizations());
        }
    }
}