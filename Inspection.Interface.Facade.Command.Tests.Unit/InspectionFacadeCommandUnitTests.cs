﻿using Framework.Application.Command;
using Framework.Core.Events;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Command.Tests.Unit
{
    public class InspectionFacadeCommandUnitTests
    {
        [Fact]
        public void
            Create_Inspection_Should_Call_Dispatch_On_Command_Bus_When_Create_Inspection_Command_Passed()
        {
            //Arrange
            var commandBus = new Mock<ICommandBus>();
            var eventListener = new Mock<IEventListener>();
            var inspectionFacade = new InspectionFacadeCommand(commandBus.Object, eventListener.Object);
            var command = CreateInspectionCommandBuilder.Single();

            //Act
            var result = inspectionFacade.CreateInspection(command);

            //Assert
            commandBus.Verify(x => x.Dispatch(command));
        }

        

        [Fact]
        public void
            Place_Question_Answers_Should_Call_Dispatch_On_Command_Bus_When_Place_QuestionAnswer_Command_Collection_Passed()
        {
            //Arrange
            var commandBus = new Mock<ICommandBus>();
            var eventListener = new Mock<IEventListener>();
            var inspectionFacade = new InspectionFacadeCommand(commandBus.Object, eventListener.Object);
            var command = PlaceQuestionAnswersBuilder.Collection();

            //Act
            inspectionFacade.PlaceQuestionAnswers(command);

            //Assert
            commandBus.Verify(x => x.Dispatch(command));
        }
    }
}