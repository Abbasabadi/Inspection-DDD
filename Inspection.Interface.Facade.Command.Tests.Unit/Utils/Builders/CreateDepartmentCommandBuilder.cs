﻿using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.Department.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class CreateDepartmentCommandBuilder
    {
        public static CreateDepartmentCommand Single()
        {
            return Builder<CreateDepartmentCommand>.CreateNew()
                .With(x=>x.Name = Faker.NameFaker.Name())
                .Build();
        }
    }
}