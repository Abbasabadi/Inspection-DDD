﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.Question.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class UpdateQuestionCommandBuilder
    {
        public static UpdateQuestionCommand Single()
        {
            return Builder<UpdateQuestionCommand>.CreateNew()
                .With(x=>x.Id == 12)
                .With(x=>x.Label == "something")
                .With(x=>x.Key == "somekey")
                .With(x => x.ControllerType == "CheckBox")
                .With(x => x.QuestionCategoryId == 100)
                .With(x => x.DepartmentId == 10)
                .With(x=>x.IsRequired == false)
                .Build();
        }
    }
}
