﻿using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.Question.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class ChangeQuestionActivationCommandBuilder
    {
        public static ChangeQuestionActivationCommand Single()
        {
            return Builder<ChangeQuestionActivationCommand>.CreateNew()
                .With(x=>x.IsActive = true)
                .With(x=>x.Id = 12)
                .Build();
        }
    }
}
