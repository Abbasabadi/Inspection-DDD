﻿using System.Collections.Generic;
using Inspection.Interface.Facade.Contracts.Inspection.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class PlaceQuestionAnswersBuilder
    {
        public static List<PlaceQuestionAnswerCommand> Collection()
        {
            return new List<PlaceQuestionAnswerCommand>
            {
                new PlaceQuestionAnswerCommand {Answer = "True", QuestionId = 1, InspectionId = 3},
                new PlaceQuestionAnswerCommand {Answer = "True", QuestionId = 2, InspectionId = 3},
                new PlaceQuestionAnswerCommand {Answer = "True", QuestionId = 3, InspectionId = 3},
            };
        }
    }
}