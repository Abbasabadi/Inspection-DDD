﻿using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class UpdateQuestionCategoryActivationCommandBuilder
    {
        public static ChangeQuestionCategoryActivationCommand Single()
        {
            return Builder<ChangeQuestionCategoryActivationCommand>.CreateNew()
                .WithConstructor(() => new ChangeQuestionCategoryActivationCommand(1,false))
                .Build();
        }
    }
}