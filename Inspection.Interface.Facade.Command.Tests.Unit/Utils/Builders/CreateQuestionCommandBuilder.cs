﻿using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.Question.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class CreateQuestionCommandBuilder
    {
        public static CreateQuestionCommand Single()
        {
            return Builder<CreateQuestionCommand>.CreateNew()
                .With(x=>x.Value = Faker.TextFaker.Sentence())
                .With(x=>x.Key = Faker.TextFaker.Sentence())
                .With(x=>x.Label = Faker.TextFaker.Sentence())
                .With(x=>x.IsRequired = Faker.BooleanFaker.Boolean())
                .With(x=>x.Order = Faker.NumberFaker.Number())
                .With(x=>x.QuestionCategoryId = Faker.NumberFaker.Number())
                .With(x=>x.DepartmentId = Faker.NumberFaker.Number())
                .With(x=>x.ControllerType = "CheckBox")
                
                .Build();
        }
    }
}