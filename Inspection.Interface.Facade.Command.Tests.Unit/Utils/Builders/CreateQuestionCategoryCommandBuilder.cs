﻿using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class CreateQuestionCategoryCommandBuilder
    {
        public static CreateQuestionCategoryCommand Single()
        {
            return Builder<CreateQuestionCategoryCommand>.CreateNew()
                .With(x => x.Name = Faker.NameFaker.Name())
                .With(x=>x.DepartmentId = Faker.NumberFaker.Number())
                .Build();
        }
    }
}