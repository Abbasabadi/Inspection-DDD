﻿using System;
using FizzWare.NBuilder;
using Inspection.Interface.Facade.Contracts.Inspection.Command;

namespace Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders
{
    public static class CreateInspectionCommandBuilder
    {
        public static CreateInspectionCommand Single()
        {
            return Builder<CreateInspectionCommand>.CreateNew()
                .With(x => x.InspectorName = "تست")
                .With(x => x.StartDate = DateTime.Today.AddDays(-2))
                .With(x => x.EndDate = DateTime.Now.AddDays(3))
                .With(x => x.OrganizationId = 23)
                .With(x => x.DepartmentId = 14)
                .Build();
        }
    }
}