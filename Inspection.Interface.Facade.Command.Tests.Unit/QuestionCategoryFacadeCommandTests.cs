﻿using Framework.Application.Command;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Inspection.Interface.Facade.Contracts.QuestionCategory.Command;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Command.Tests.Unit
{
    public class QuestionCategoryFacadeCommandTests
    {
        private readonly Mock<ICommandBus> _commandBus;
        private readonly QuestionCategoryFacadeCommand _questionCategoryFacadeCommand;

        public QuestionCategoryFacadeCommandTests()
        {
            _commandBus = new Mock<ICommandBus>();
            _questionCategoryFacadeCommand = new QuestionCategoryFacadeCommand(_commandBus.Object);
        }

        [Fact]
        public void Create_Should_Call_Dispatch_Category_On_Command_Bus()
        {
            //Arrange
            var command = CreateQuestionCategoryCommandBuilder.Single();

            //Act
            _questionCategoryFacadeCommand.Create(command);

            //Assert
            _commandBus.Verify(x => x.Dispatch(command));
        }

        [Fact]
        public void UpdateActivationStatus_Shoult_Call_Dispatch_On_Command_Bus()
        {
            //Arrange
            var command = UpdateQuestionCategoryActivationCommandBuilder.Single();

            //Act
            _questionCategoryFacadeCommand.UpdateActivationStatus(command);

            //Assert
            _commandBus.Verify(x => x.Dispatch(command));
        }
    }
}