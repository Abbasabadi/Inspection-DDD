﻿using Framework.Application.Command;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Command.Tests.Unit
{
    public class DepartmentFacadeCommandTests
    {
        [Fact]
        public void Create_Department_Should_Call_Dispatch_On_Command_Bus()
        {
            //Arrange
            var commandBus = new Mock<ICommandBus>();
            var departmentFacade = new DepartmentFacadeCommand(commandBus.Object);
            var command = CreateDepartmentCommandBuilder.Single();

            //Act
            departmentFacade.CreateDepartment(command);

            //Assert
            commandBus.Verify(x=>x.Dispatch(command));
        }
    }
}
