﻿using Framework.Application.Command;
using Inspection.Interface.Facade.Command.Tests.Unit.Utils.Builders;
using Moq;
using Xunit;

namespace Inspection.Interface.Facade.Command.Tests.Unit
{
    public class QuestionFacadeCommandTests
    {
        private readonly Mock<ICommandBus> _commandBus;
        private readonly QuestionFacadeCommand _questionFacadeCommand;

        public QuestionFacadeCommandTests()
        {
            _commandBus = new Mock<ICommandBus>();
            _questionFacadeCommand = new QuestionFacadeCommand(_commandBus.Object);
        }

        [Fact]
        public void Create_Question_Should_Call_Dispatch_On_Command_Bus()
        {
            //Arrange
            var command = CreateQuestionCommandBuilder.Single();

            //Act
            _questionFacadeCommand.CreateQuestion(command);

            //Assert
            _commandBus.Verify(x => x.Dispatch(command));
        }

        [Fact]
        public void ChangeQuestionActivation_Should_Call_Dispatch_On_Command_Buss()
        {
            //Arrange
            var command = ChangeQuestionActivationCommandBuilder.Single();

            //Act
            _questionFacadeCommand.ChangeQuestionActivation(command);

            //Assert
            _commandBus.Verify(x => x.Dispatch(command));
        }

        [Fact]
        public void UpdateQuestion_Should_Call_Dispatch_On_Command_Bus()
        {
            //Arrange
            var command = UpdateQuestionCommandBuilder.Single();

            //Act
            _questionFacadeCommand.UpdateQuestion(command);

            //Assert
            _commandBus.Verify(x=>x.Dispatch(command));
        }
    }
}