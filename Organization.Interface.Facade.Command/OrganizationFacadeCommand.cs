﻿using Framework.Application.Command;
using Organization.Interface.Facade.Contract.Organizations.Command;

namespace Organization.Interface.Facade.Command
{
    public class OrganizationFacadeCommand: IOrganizationFacadeCommand
    {
        private readonly ICommandBus _commandBus;

        public OrganizationFacadeCommand(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public void CreateOrganization(CreateOrganizationCommand command)
        {
            _commandBus.Dispatch(command);
        }
    }
}
