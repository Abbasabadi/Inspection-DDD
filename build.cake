﻿var target = Argument("target", "Default");

Task("Restore-NuGet-Packages")
    .Does(() =>
{
    //NuGetRestore("./Inspection(CSIS)-DDD.sln");
});


Task("Default")
    .IsDependentOn("xUnit");
	
Task("Build")
    .IsDependentOn("Restore-NuGet-Packages")
    .Does(() =>
{
  // MSBuild("./Inspection(CSIS)-DDD.sln");   
});

Task("xUnit")
    .IsDependentOn("Build")
    .Does(() =>
{
   XUnit2("./Organization.Interface.Rest.Api.Tests/bin/Debug/Organization.Interface.Rest.Api.Tests.dll");
});


RunTarget(target);