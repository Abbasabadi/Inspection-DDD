﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Query.Mongo.Department
{
    public class DepartmentQuery : IDepartmentQuery
    {
        private readonly IMongoCollection<Domain.Models.Department.Department> _departmentCollection;

        public DepartmentQuery(IMongoDatabase database)
        {
            _departmentCollection = database.GetCollection<Domain.Models.Department.Department>("Departments");
        }

        public List<Domain.Models.Department.Department> GetDepartments()
        {
            var filter = Builders<Domain.Models.Department.Department>.Filter.Empty;
            return _departmentCollection.Find(filter).ToList();
        }

        public Domain.Models.Department.Department GetDepartmentBy(long id)
        {
            var filter = Builders<Domain.Models.Department.Department>.Filter.Eq(x => x.Id.DbId, id);
            return _departmentCollection.Find(filter).First();
        }
    }
}