﻿using System.Collections.Generic;

namespace Inspection.Infrastructure.Query.Mongo.Department
{
    public interface IDepartmentQuery
    {
        List<Domain.Models.Department.Department> GetDepartments();
        Domain.Models.Department.Department GetDepartmentBy(long id);
    }
}
