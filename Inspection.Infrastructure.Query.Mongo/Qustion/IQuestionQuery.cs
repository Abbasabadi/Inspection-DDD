﻿using System.Collections.Generic;
using Inspection.Domain.Models.Question;

namespace Inspection.Infrastructure.Query.Mongo.Qustion
{
    public interface IQuestionQuery
    {
        List<Question> GetQuestionsByQuestionCategoryId(long questionCategoryId);
        Question GetQuestionBy(long questionId);

        List<Question> GetQuestions();
    }
}
