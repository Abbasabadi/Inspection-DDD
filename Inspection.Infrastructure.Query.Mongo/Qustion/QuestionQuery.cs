﻿using System.Collections.Generic;
using Inspection.Domain.Models.Question;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Query.Mongo.Qustion
{
    public class QuestionQuery : IQuestionQuery
    {
        private readonly IMongoCollection<Question> _questionCollection;

        public QuestionQuery(IMongoDatabase database)
        {
            _questionCollection = database.GetCollection<Question>("Questions");
        }

        public List<Question> GetQuestionsByQuestionCategoryId(long questionCategoryId)
        {
            var filter = Builders<Question>.Filter.And(
                Builders<Question>.Filter.Eq(x => x.QuestionCategoryId.Value, questionCategoryId),
                Builders<Question>.Filter.Eq(x => x.IsActive, true));
            return _questionCollection.Find(filter).ToList();
        }

        public Question GetQuestionBy(long questionId)
        {
            var filter = Builders<Question>.Filter.Eq(x => x.Id.DbId, questionId);
            return _questionCollection.Find(filter).First();
        }

        public List<Question> GetQuestions()
        {
            var filter = Builders<Question>.Filter.Empty;
            return _questionCollection.Find(filter).ToList();
        }
    }   
}