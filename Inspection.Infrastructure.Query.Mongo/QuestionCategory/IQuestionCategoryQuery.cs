﻿using System.Collections.Generic;

namespace Inspection.Infrastructure.Query.Mongo.QuestionCategory
{
    public interface IQuestionCategoryQuery
    {
        List<Domain.Models.QuestionCategory.QuestionCategory> GetQuestionCategoriesBy(long departmentId);
        List<Domain.Models.QuestionCategory.QuestionCategory> GetQuestionCategories();
        Domain.Models.QuestionCategory.QuestionCategory GetQuestionCategoryBy(long id);
    }
}
