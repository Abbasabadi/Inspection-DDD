﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Query.Mongo.QuestionCategory
{
    public class QuestionCategoryQuery : IQuestionCategoryQuery
    {
        private readonly IMongoCollection<Domain.Models.QuestionCategory.QuestionCategory> _questionCategoryCollection;

        public QuestionCategoryQuery(IMongoDatabase database)
        {
            _questionCategoryCollection =
                database.GetCollection<Domain.Models.QuestionCategory.QuestionCategory>("QuestionCategories");
        }

        public List<Domain.Models.QuestionCategory.QuestionCategory> GetQuestionCategoriesBy(
            long departmentId)
        {
            var filter =
                Builders<Domain.Models.QuestionCategory.QuestionCategory>.Filter.Eq(x => x.DepartmentId.Value,
                    departmentId);
            return _questionCategoryCollection.Find(filter).ToList();
        }

        public List<Domain.Models.QuestionCategory.QuestionCategory> GetQuestionCategories()
        {
            var filter = Builders<Domain.Models.QuestionCategory.QuestionCategory>.Filter.Empty;
            return _questionCategoryCollection.Find(filter).ToList();
        }

        public Domain.Models.QuestionCategory.QuestionCategory GetQuestionCategoryBy(long id)
        {
            var filter = Builders<Domain.Models.QuestionCategory.QuestionCategory>.Filter.Eq(x => x.Id.DbId, id);
            return _questionCategoryCollection.Find(filter).First();
        }
    }
}
