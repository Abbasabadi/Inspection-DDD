﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace Inspection.Infrastructure.Query.Mongo.Inspection
{
    public class InspectionQuery : IInspectionQuery
    {
        private readonly IMongoCollection<Domain.Models.Inspection.Inspection> _inspectionCollection;

        public InspectionQuery(IMongoDatabase database)
        {
            _inspectionCollection = database.GetCollection<Domain.Models.Inspection.Inspection>("Inspections");
        }

        public Domain.Models.Inspection.Inspection GetInspection(long inspectionId)
        {
            var filter = Builders<Domain.Models.Inspection.Inspection>.Filter.Eq(x => x.Id.DbId, inspectionId);
            return _inspectionCollection.Find(filter).Single();
        }

        public List<Domain.Models.Inspection.Inspection> GetInspections()
        {
            return _inspectionCollection.Find(FilterDefinition<Domain.Models.Inspection.Inspection>.Empty).ToList();
        }
    }
}