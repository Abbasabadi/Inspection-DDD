﻿using System.Collections.Generic;

namespace Inspection.Infrastructure.Query.Mongo.Inspection
{
    public interface IInspectionQuery
    {
        Domain.Models.Inspection.Inspection GetInspection(long inspectionId);
        List<Domain.Models.Inspection.Inspection> GetInspections();
    }
}
