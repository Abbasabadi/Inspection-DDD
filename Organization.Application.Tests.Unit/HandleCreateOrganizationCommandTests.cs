﻿using Moq;
using Organization.Domain.Models.Organizations;
using Organization.Interface.Facad.Command.Tests.Unit.CommandBuilders;
using Xunit;

namespace Organization.Application.Tests.Unit
{
    public class HandleCreateOrganizationCommandTests
    {
        [Fact]
        public void Should_Call_GetNextId_And_CreateOrganization_On_Organization_Repository_When_Create_Organization_Command_Passed()
        {
            //Arrange
            var organizationRepository = new Mock<IOrganizationRepository>();
            var organizationCommandHandler = new OrganizationCommandHandler(organizationRepository.Object);
            var command = CreateOrganizationCommandBuilder.Single();

            //Act
            organizationCommandHandler.Handle(command);

            //Assert
            organizationRepository.Verify(x=>x.GetNextId());
            organizationRepository.Verify(x=>x.CreataOrganization(It.IsAny<Domain.Models.Organizations.Organization>()));
        }
    }
}
