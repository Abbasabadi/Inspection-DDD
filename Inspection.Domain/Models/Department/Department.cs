﻿using System;
using Framework.Domain;

namespace Inspection.Domain.Models.Department
{
    public class Department : EntityBase<DepartmentId>, IAggregateRoot
    {
        public string Name { get; private set; }
        public bool IsActive { get; private set; }

        public Department(DepartmentId id, string name) : base(id)
        {
            GaurdAgainstNullOrEmptyName(name);

            Name = name;
            CreationDateTime = DateTime.Now;
            IsActive = true;
        }

        private static void GaurdAgainstNullOrEmptyName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException();
        }

        public void ChangeActivation(bool isActive)
        {
            IsActive = isActive;
        }
    }
}