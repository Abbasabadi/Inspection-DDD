﻿using Framework.Domain;

namespace Inspection.Domain.Models.Department
{
    public class DepartmentId: IdBase<long>
    {
        public DepartmentId(long idDbId) : base(idDbId)
        {
        }
    }
}
