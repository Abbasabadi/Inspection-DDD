﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Domain.Models.Department
{
    public interface IDepartmentRepository
    {
        void CreateDepartment(Department department);
        long GetNextId();
    }
}
