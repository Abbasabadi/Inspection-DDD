﻿using Framework.Domain;

namespace Inspection.Domain.Models.QuestionCategory
{
    public class QuestionCategoryId: IdBase<long>
    {
        public QuestionCategoryId(long idDbId) : base(idDbId)
        {
        }
    }
}
