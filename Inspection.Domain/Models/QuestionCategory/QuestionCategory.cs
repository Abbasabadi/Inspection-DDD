﻿using System;
using Framework.Core.Events;
using Framework.Domain;
using Inspection.Domain.Models.Shared;

namespace Inspection.Domain.Models.QuestionCategory
{
    public class QuestionCategory : EntityBase<QuestionCategoryId>, IAggregateRoot
    {
        public string Name { get; private set; }
        public DepartmentId DepartmentId { get; private set; }
        public bool IsActive { get; private set; }

        public QuestionCategory(QuestionCategoryId id, string name, DepartmentId departmentId) : base(id)
        {
            GaurdAgainstNullOrEmptyName(name);

            Name = name;
            DepartmentId = departmentId;
            IsActive = true;
            CreationDateTime = DateTime.Now;
        }

        public void ChangeActivationStatus(bool isActive)
        {
            IsActive = isActive;
        }

        public void Modify(string name, long departmentId)
        {
            GaurdAgainstNullOrEmptyName(name);

            Name = name;
            DepartmentId = new DepartmentId(departmentId);
        }

        private static void GaurdAgainstNullOrEmptyName(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException();
        }
    }
}