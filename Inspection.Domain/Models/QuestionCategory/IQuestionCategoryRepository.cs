﻿namespace Inspection.Domain.Models.QuestionCategory
{
    public interface IQuestionCategoryRepository
    {
        void Create(QuestionCategory questionCategory);
        QuestionCategory GetBy(long questionCategoryId);
        void Update(QuestionCategory questionCategory);
        long GetNextId();
    }
}
