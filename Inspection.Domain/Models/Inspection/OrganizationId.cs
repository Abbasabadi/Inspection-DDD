﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Domain;

namespace Inspection.Domain.Models.Inspection
{
    public class OrganizationId: ValueObjectBase
    {
        public long Value { get; set; }

        public OrganizationId(long value)
        {
            Value = value;
        }
    }
}
