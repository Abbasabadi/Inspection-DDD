﻿using Framework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Core.Events;
using Inspection.Domain.Contract;
using Inspection.Domain.Models.Inspection.Exceptions;
using Inspection.Domain.Models.Shared;

namespace Inspection.Domain.Models.Inspection
{
    public class Inspection : EntityBase<InspectionId>, IAggregateRoot
    {
        public List<QuestionAnswer> QuestionAnswers { get; set; }

        public string InspectorName { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public OrganizationId OrganizationId { get; private set; }
        public DepartmentId DepartmentId { get; private set; }
        //public IList<QuestionAnswer> QuestionAnswers => _questionAnswers.AsReadOnly();

        public Inspection(InspectionId id, string inspectorName, DateTime startDate,
            DateTime endDate, OrganizationId organizationId, DepartmentId departmentId, IEventPublisher eventPublisher)
            : base(id, eventPublisher)
        {
            GaurdAgainstStartDateBiggerThanEndDate(startDate, endDate);
            GaurdAgainstInvalidStartDate(startDate);
            //TODO: gaurd against invalid data
            QuestionAnswers = new List<QuestionAnswer>();
            InspectorName = inspectorName;
            StartDate = startDate;
            EndDate = endDate;
            OrganizationId = organizationId;
            DepartmentId = departmentId;
            CreationDateTime = DateTime.Now;
            eventPublisher.Publish(new InspectionCreated(this.Id.DbId));
            //Publish(id);
        }

        private void Publish(InspectionId id)
        {
            EventPublisher.Publish(new InspectionCreated(id.DbId));
        }

        public void PlaceQuestionAnswers(List<QuestionAnswer> questionAnswers)
        {
            foreach (var questionAnswer in questionAnswers)
            {
                try
                {
                    var currentQuestion = QuestionAnswers.Single(q => q.QuestionId == questionAnswer.QuestionId);
                    currentQuestion.Answer = questionAnswer.Answer;
                }
                catch
                {
                    QuestionAnswers.Add(questionAnswer);
                }
            }
        }

        private static void GaurdAgainstStartDateBiggerThanEndDate(DateTime startDate,
            DateTime endDate)
        {
            if (startDate > endDate)
                throw new StartDateIsBiggerThanEndDateException();
        }

        private static void GaurdAgainstInvalidStartDate(DateTime startDate)
        {
            if (startDate > DateTime.Today)
                throw new StartDateBiggerThanTodayException();
        }
    }
}