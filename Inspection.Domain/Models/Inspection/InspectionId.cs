﻿using Framework.Domain;

namespace Inspection.Domain.Models.Inspection
{
    public class InspectionId : IdBase<long>
    {
        public InspectionId(long idDbId) : base(idDbId)
        {
        }
    }
}