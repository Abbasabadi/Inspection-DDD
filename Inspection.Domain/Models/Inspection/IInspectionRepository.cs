﻿namespace Inspection.Domain.Models.Inspection
{
    public interface IInspectionRepository
    {
        void CreateInspection(Inspection inspection);
        void UpdateInspectionAnswers(Inspection inspection);
        Inspection GetById(long id);
        long GetNextId();
    }
}
