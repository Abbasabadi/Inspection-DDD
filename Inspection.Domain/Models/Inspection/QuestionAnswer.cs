﻿using Framework.Domain;

namespace Inspection.Domain.Models.Inspection
{
    public class QuestionAnswer : ValueObjectBase
    {
        public QuestionAnswer(string answer, long questionId)
        {
            Answer = answer;
            QuestionId = questionId;
        }

        public string Answer { get; set; }
        public long QuestionId { get; set; }
    }
}
