﻿using Framework.Domain;

namespace Inspection.Domain.Models.Question
{
    public class QuestionCategoryId : ValueObjectBase
    {
        public long Value { get; private set; }

        public QuestionCategoryId(long value)
        {
            Value = value;
        }
    }
}