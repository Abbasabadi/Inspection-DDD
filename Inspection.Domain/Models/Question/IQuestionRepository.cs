﻿namespace Inspection.Domain.Models.Question
{
    public interface IQuestionRepository
    {
        void CreateQuestion(Question question);
        void UpdateQuesiton(Question question);
        //void ChangeQuestionActivation(long questionId, bool isActive);
        Question GetBy(long id);
        long GetNextId();
    }
}
