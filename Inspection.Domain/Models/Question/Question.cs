﻿using System;
using Framework.Domain;
using Inspection.Domain.Models.Shared;

namespace Inspection.Domain.Models.Question
{
    public class Question : EntityBase<QuestionId>, IAggregateRoot
    {
        public string Key { get; private set; }
        public string Label { get; private set; }
        public bool IsRequired { get; private set; }
        public int Order { get; private set; }
        public bool IsActive { get; private set; }
        public ControllerType ControllerType { get; private set; }
        public DepartmentId DepartmentId { get; private set; }
        public QuestionCategoryId QuestionCategoryId { get; private set; }

        public Question(QuestionId id, string key, string label, bool isRequired, int order, bool isActive,
            long departmentId, long questionCategoryId, string controllerType) : base(id)
        {
            Key = key;
            Label = label;
            IsRequired = isRequired;
            Order = order;
            IsActive = isActive;
            DepartmentId = new DepartmentId(departmentId);
            QuestionCategoryId = new QuestionCategoryId(questionCategoryId);
            ControllerType = new ControllerType(controllerType);
            CreationDateTime = DateTime.Now;
        }

        public void ChangeQuestionActivation(bool isActive)
        {
            IsActive = isActive;
        }

        public void Modify(long departmentId, long questionCategoryId, string controllerType, string label, string key, bool isRequired)
        {
            DepartmentId = new DepartmentId(departmentId);
            QuestionCategoryId = new QuestionCategoryId(questionCategoryId);
            ControllerType = new ControllerType(controllerType);
            Label = label;
            Key = key;
            IsRequired = isRequired;
        }
    }
}