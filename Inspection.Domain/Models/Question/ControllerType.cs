﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Domain;

namespace Inspection.Domain.Models.Question
{
    public class ControllerType : ValueObjectBase
    {
        public string Value { get; set; }

        public ControllerType(string contorllerType)
        {
            Value = contorllerType;
        }
    }
}