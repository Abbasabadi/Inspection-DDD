﻿using Framework.Domain;

namespace Inspection.Domain.Models.Question
{
    public class QuestionId : IdBase<long>
    {
        public QuestionId(long idDbId) : base(idDbId)
        {
        }
    }
}
