﻿using Framework.Domain;

namespace Inspection.Domain.Models.Shared
{
    public class DepartmentId: ValueObjectBase
    {
        public long Value { get; private set; }

        public DepartmentId(long value)
        {
            Value = value;
        }
    }
}
