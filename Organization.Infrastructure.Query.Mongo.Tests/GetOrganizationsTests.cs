﻿using System;
using MongoDB.Driver;
using Orgnaization.Infrastructure.Query.Mongo;
using Xunit;

namespace Organization.Infrastructure.Query.Mongo.Tests
{
    public class GetOrganizationsTests : IDisposable
    {
        private IMongoDatabase _database;

        public GetOrganizationsTests()
        {
            _database = new MongoClient().GetDatabase("CSIS");
        }

        [Fact]
        public void Should_Return_All_Organizations()
        {
            var organizationQuery = new OrganizationQuery(_database);
            var organizations = organizationQuery.GetOrganizations();
            Assert.InRange(organizations.Count, 0, long.MaxValue);
        }

        public void Dispose()
        {
            _database = null;
        }
    }
}