﻿using System;
using MongoDB.Driver;
using Organization.Domain.Tests.Unit.Organization;
using Orgnaization.Infrastructure.Query.Mongo;
using Xunit;

namespace Organization.Infrastructure.Query.Mongo.Tests
{
    public class GetOrganizationTests: IDisposable
    {
        private IMongoDatabase _database;
        private Domain.Models.Organizations.Organization _organization;

        public GetOrganizationTests()
        {
            _database = new MongoClient().GetDatabase("CSIS");
        }

        [Fact]
        public void Should_Return_Single_Organization_When_Organization_Id_Is_Passed()
        {
            _organization = new OrganizationBuilder().WithId(6).Build();
            var organizationQuery = new OrganizationQuery(_database);
            var organization = organizationQuery.GetOrganization(_organization.Id.DbId);
            Assert.NotNull(organization.Name);
            Assert.IsType<Domain.Models.Organizations.Organization>(_organization);
        }

        public void Dispose()
        {
            _organization = null;
            _database = null;
        }
    }
}