﻿using System;
using Xunit;

namespace Organization.Domain.Tests.Unit.Organization
{
    public class OrganizationTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Cant_Create_Organization_When_Organization_Name_Is_Null_Or_Empty(string organizationName)
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                var organization = new OrganizationBuilder().WithName(organizationName).Build();
            });
        }

        [Fact]
        public void Can_Create_Organization_With_Default_Data()
        {
            var organization = new OrganizationBuilder().Build();
            Assert.NotEmpty(organization.Name);
            Assert.True(organization.IsHeadQuarter);
        }
    }
}