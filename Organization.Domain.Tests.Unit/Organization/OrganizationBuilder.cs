﻿using System.Collections.Generic;
using Framework.Core;
using Organization.Domain.Models.Organizations;

namespace Organization.Domain.Tests.Unit.Organization
{
    public class OrganizationBuilder: IBuilder<Models.Organizations.Organization>
    {
        public long Id;
        public string Name;
        public bool IsHeadQuarter;

        public OrganizationBuilder()
        {
            Id = 1;
            Name = "سازمان بورس و اوراق بهادار";
            IsHeadQuarter = true;
        }

        public OrganizationBuilder WithId(long id)
        {
            Id = id;
            return this;
        }

        public OrganizationBuilder WithName(string name)
        {
            Name = name;
            return this;
        }

        public OrganizationBuilder WithIsHeadQuarter(bool isHeadQuarter)
        {
            IsHeadQuarter = isHeadQuarter;
            return this;
        }

        public Models.Organizations.Organization Build()
        {
            var organizationId = new OrganizationId(Id);
            return new Models.Organizations.Organization(organizationId, Name, IsHeadQuarter);
        }

        public List<Models.Organizations.Organization> BuildCollection()
        {
            return new List<Models.Organizations.Organization>()
            {
                Build(),
                Build(),
                Build()
            };
        }
    }
}
