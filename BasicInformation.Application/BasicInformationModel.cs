﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace BasicInformation.Application
{
    public class BasicInformationModel
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public List<ControllerType> Members { get; set; }
    }

    public class ControllerType
    {
        public string Name { get; set; }
    }
}
