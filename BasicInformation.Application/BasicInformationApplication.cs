﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace BasicInformation.Application
{
    public class BasicInformationApplication
    {
        private readonly IMongoCollection<BasicInformationModel> _mongoCollection;

        public BasicInformationApplication()
        {
            _mongoCollection = new MongoClient().GetDatabase("CSIS").GetCollection<BasicInformationModel>("BasicInformation");
        }

        public List<ControllerType> GetControllerTypes()
        {
            var filter = Builders<BasicInformationModel>.Filter.Eq(model => model.Name, "ControllerTypes");
            var result = _mongoCollection.Find(filter).Single();
            return result.Members;
        }
    }
}
